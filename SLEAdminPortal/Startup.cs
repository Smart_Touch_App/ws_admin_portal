﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SLEAdminPortal.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace SLEAdminPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
