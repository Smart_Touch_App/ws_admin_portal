﻿using sampleweb;
using SLEAdminPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal.Controllers
{
    public class WindowsServiceController : Controller
    {
        public ActionResult Index()
        {
            try
            {

                var EnvList = new Managers.RouteSecurityManager().GetEnvDdl();
                ViewData["WSEnvironments"] = new SelectList(EnvList.OrderBy(x => x.EnvName), "EnvName", "EnvName");
               

                //IList<SLEAdminPortal.Models.Environment> EnvList = new List<SLEAdminPortal.Models.Environment>();
                //EnvList.Add(new SLEAdminPortal.Models.Environment() { EnvironmentName = "Prototype-DV" });
                //EnvList.Add(new SLEAdminPortal.Models.Environment() { EnvironmentName = "Prototype-PYD" });
                //EnvList.Add(new SLEAdminPortal.Models.Environment() { EnvironmentName = "Prototype-PYU" });
                //ViewData["WSEnvironments"] = new SelectList(EnvList, "EnvironmentName", "EnvironmentName");

                List<SelectListItem> DailyFrequentList = new List<SelectListItem>();
                DailyFrequentList.Add(new SelectListItem { Text = "Every 20 Mins", Value = "1", Selected = true });
                DailyFrequentList.Add(new SelectListItem { Text = "Every 1 Hour", Value = "2" });
                DailyFrequentList.Add(new SelectListItem { Text = "Every 3 Hours", Value = "3" });
                DailyFrequentList.Add(new SelectListItem { Text = "Every 12 Hours", Value = "4" });
                ViewData["Frequents"] = new SelectList(DailyFrequentList, "Text", "Text");


                List<SelectListItem> WeekDaysList = new List<SelectListItem>();
                WeekDaysList.Add(new SelectListItem { Text = "Sunday", Value = "1", Selected = true });
                WeekDaysList.Add(new SelectListItem { Text = "Monday", Value = "2" });
                WeekDaysList.Add(new SelectListItem { Text = "Tuesday", Value = "3" });
                WeekDaysList.Add(new SelectListItem { Text = "Wednesday", Value = "4" });
                WeekDaysList.Add(new SelectListItem { Text = "Thursday", Value = "5" });
                WeekDaysList.Add(new SelectListItem { Text = "Friday", Value = "6" });
                WeekDaysList.Add(new SelectListItem { Text = "Saturday", Value = "7" });
                ViewData["WeekDays"] = new SelectList(WeekDaysList, "Text", "Text");

                List<SelectListItem> RouteList = new List<SelectListItem>();
                RouteList.Add(new SelectListItem { Text = "Sales Order", Value = "1", Selected = true });
                RouteList.Add(new SelectListItem { Text = "Replenishment", Value = "2" });
                RouteList.Add(new SelectListItem { Text = "Receipts", Value = "3" });
                ViewData["Modules"] = new SelectList(RouteList, "Value", "Text");

                List<SelectListItem> RouteList1 = new List<SelectListItem>();
                RouteList1.Add(new SelectListItem { Text = "SalesOrder Status", Value = "1", Selected = true });
                RouteList1.Add(new SelectListItem { Text = "SalesOrder Process", Value = "2" });
                RouteList1.Add(new SelectListItem { Text = "Receipts Status", Value = "3" });
                RouteList1.Add(new SelectListItem { Text = "Receipts Process", Value = "4" });
                RouteList1.Add(new SelectListItem { Text = "Replenishment Status", Value = "5" });
                RouteList1.Add(new SelectListItem { Text = "Replenishment Process", Value = "6" });
                ViewData["SubModules"] = new SelectList(RouteList1, "Value", "Text");


                List<SelectListItem> DependencyList = new List<SelectListItem>();
                DependencyList.Add(new SelectListItem { Text = "Y", Value = "1", Selected = true });
                DependencyList.Add(new SelectListItem { Text = "N", Value = "2" });
                ViewData["Dependencys"] = new SelectList(DependencyList, "Value", "Text");


                //List<SelectListItem> EmailGroupList = new List<SelectListItem>();
                //EmailGroupList.Add(new SelectListItem { Text = "Replenishment Group", Value = "1", Selected = true });
                //EmailGroupList.Add(new SelectListItem { Text = "Sales Order Group", Value = "2" });
                //EmailGroupList.Add(new SelectListItem { Text = "Receipts Group", Value = "3" });

                //ViewData["EmailGroups"] = new SelectList(EmailGroupList, "Value", "Text");

                ServiceModel models = new ServiceModel();
                models.JobDetails = new Managers.ServiceManager().GetjobDetails();
                models.EffectiveFrom = DateTime.Now;
                models.EffectiveTo = DateTime.Now;
                var JobNameList = new Managers.ServiceManager().GetJobNameDdl();
                models.EmailConfigDetails = new Managers.ServiceManager().GetConfigDetails();
                var EmailGroup = new Managers.ServiceManager().GetEmailGroupDdl();
                string emailoption = "";
                foreach (var item in EmailGroup)
                {
                    emailoption = emailoption + item.ID + "-" + item.GroupEmailName + "_";
                }               
                ViewBag.emailoption = emailoption;
                ViewData["JobName"] = new SelectList(JobNameList.OrderBy(x => x.JobName), "JobID", "JobName");
                models.Services = new Managers.ServiceManager().GetServiceScheduleList();
                models.ImmediateServices = new Managers.ServiceManager().GetImmediateList();
                models.ServiceHistorys = new Managers.ServiceManager().GetServiceList();
                bool IsRunning = new Managers.ServiceManager().ServiceStatus();
                models.IsServiceRunning = IsRunning;
                return View("Index", models);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult GetJobDetails(int JobID)
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.ModuleSequences = new Managers.ServiceManager().GetJobDetailsList(JobID);
                for (int i = 0; i < sc.ModuleSequences.Count();i++ )
                {
                    if(sc.ModuleSequences[i].Dependency=="True")
                    {
                        sc.ModuleSequences[i].Dependency = "Y";
                    }
                    else
                    {
                        sc.ModuleSequences[i].Dependency = "N";
                    }
                    if (sc.ModuleSequences[i].EmailNotification == "True")
                    {
                        sc.ModuleSequences[i].EmailNotification = "Y";
                    }
                    else
                    {
                        sc.ModuleSequences[i].EmailNotification = "N";
                    }
                }
                    return PartialView("_JobDetailsList", sc);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        //public ActionResult StartService()
        //{

        //    int execute = new Managers.ServiceManager().ExecutionStatus();


        //    if (execute != 1)
        //    {
        //        ServiceController svcController = new ServiceController("DTService");

        //        if (svcController != null)
        //        {
        //            try
        //            {

        //                if (svcController.Status.ToString() == "Running")
        //                {
        //                    svcController.Stop();
        //                    svcController.Start();
        //                }
        //                else
        //                    svcController.Start();
        //            }
        //            catch (Exception ex)
        //            {
        //                //Todo: throw new application exeception
        //            }

        //        }
        //        // servicecontroller svccontroller = new servicecontroller("DTService"); ////rand.packetdownload is the service name
        //        //using (Impersonation imperson = new Impersonation("techunison", "marimuthu", "welcome@123"))
        //        //{
        //        //    ServiceController svccontroller = new ServiceController("DTService", "192.168.1.124");
        //        //    if (svccontroller.Status.ToString() == "running")
        //        //    {
        //        //        svccontroller.Stop();
        //        //        svccontroller.Start();
        //        //    }
        //        //    else
        //        //    {
        //        //        svccontroller.Start();
        //        //    }
        //        //}
        //    }
        //    else
        //    {
        //        int Waiting = new Managers.ServiceManager().UpdateWaiting();
        //    }


        //    return RedirectToAction("WindowsServiceIndex");
        //}

        public ActionResult UpdateScheduleList()
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.Services = new Managers.ServiceManager().GetServiceScheduleList();
                return PartialView("_ScheduleList", sc);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult UpdateImmediateList()
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.ImmediateServices = new Managers.ServiceManager().GetImmediateList();
                return PartialView("_ImmediateList", sc);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult UpdateEmailConfigDetails()
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.EmailConfigDetails = new Managers.ServiceManager().GetConfigDetails();
                return PartialView("_EmailConfigList", sc);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetScheduleDetails(int ScheduleID)
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.Services = new Managers.ServiceManager().GetScheduleDetailsList(ScheduleID);
                
                return PartialView("_ScheduleDetailsList", sc);
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
        }

        public ActionResult GetImmediateDetails(int JobID)
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.ImmediateServices = new Managers.ServiceManager().GetImmediateDetailsList(JobID);

                return PartialView("_ImmediateDetailsList", sc);
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
        }


        public ActionResult GetServiceHistoryDetails(int HistoryID)
        {
            try
            {
                ServiceModel sc = new ServiceModel();
                sc.ModuleSequences = new Managers.ServiceManager().GetServicHistoryDetailsList(HistoryID);
                for (int i = 0; i < sc.ModuleSequences.Count(); i++)
                {
                    if (sc.ModuleSequences[i].Dependency == "True")
                    {
                        sc.ModuleSequences[i].Dependency = "Y";
                    }
                    else
                    {
                        sc.ModuleSequences[i].Dependency = "N";
                    }
                    if (sc.ModuleSequences[i].EmailNotification == "True")
                    {
                        sc.ModuleSequences[i].EmailNotification = "Y";
                    }
                    else
                    {
                        sc.ModuleSequences[i].EmailNotification = "N";
                    }
                }
                return PartialView("_ServiceHistoryDetails", sc);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult SaveJob(List<JobDetails> param)
        {
            try
            {
                bool result = new Managers.ServiceManager().SaveJobSequence(param);
                if (result)
                {
                    return Json(new { msg = "Record Saved", result = true });
                }
                else
                {
                    return Json(new { msg = "Job Name Already Exists", result = false });
                }
            
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Already Exists", result = false });
            }

        }


        public JsonResult SaveSchedule(Service sc)
        {
            try
            {
                bool result = new Managers.ServiceManager().SaveNewSchedule(sc);
                return Json(new  { msg = "Record Saved", result = true });
            }
            catch
            {
                return Json(new { msg = "Record Not Saved", result = false });
            }
        }

        public JsonResult GetScheduleToEdit(int ScheduleID)
        {
            try
            {
                Service EditScheduleDetails = new Managers.ServiceManager().GetEditSchedule(ScheduleID);
                return Json(new { Data = EditScheduleDetails, result = true });
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Already Exists", result = false });
            }

        }

        public JsonResult UpdateSchedule(Service sc)
        {
            try
            {
                bool result = new Managers.ServiceManager().UpdateSchedule(sc);
                return Json(new { msg = "Record Updated", result = true });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public JsonResult SaveImmediate(Service sc)
        {
            try
            {
                bool result = new Managers.ServiceManager().SaveImmediateSchedule(sc);
                if (result)
                {
                    //Start 
                    //ServiceController svcController = new ServiceController("DTService");
                    //if (svcController.Status.ToString() == "Running")
                    //{
                    //    svcController.Stop();
                    //    Thread.Sleep(2000);
                    //    svcController.Start();
                    //}
                    //else
                    //{
                    //    svcController.Start();
                    //    Thread.Sleep(2000);
                    //}
                    //End


                    using (Impersonation imperson = new Impersonation("fbctorrance", "mobdev3", "torrdev1"))
                    {
                        ServiceController svccontroller = new ServiceController("DTService", "10.30.206.171");
                        if (svccontroller.Status.ToString() == "Running")
                        {
                            svccontroller.Stop();
                            Thread.Sleep(2000);
                            svccontroller.Start();
                        }
                        else
                        {
                            svccontroller.Start();
                        }
                    }
                }
                return Json(new { msg = "Record Saved", result = true });
            }
            catch(Exception ex)
            {
                return Json(new { msg = ex.Message, result = false });
            }
        }

        public JsonResult GetJobtoEdit(string jodid)
        {
            try
            {
                List<JobDetails> JobDetail = new Managers.ServiceManager().GetJobtoEdit(jodid);

                return Json(new { Data = JobDetail, result = true });
            
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Already Exists", result = false });
            }

        }


        public JsonResult ChangeActiveFlag(string jodid, string Active)
        {
            try
            {

                new Managers.ServiceManager().UpdateActive(jodid, Active);
                return Json(new { result = true });
            }
            catch(Exception ex)
            {
                return Json(new { result = false });
            }
        }

       


        public JsonResult DeleteEmailConfig(string ID)
        {
            try
            {
                new Managers.ServiceManager().DeleteEmailConfig(ID);
                return Json(new { msg = "Record Deleted", result = true });
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Not Deleted", result = false });
            }

        }

        public JsonResult GetEmailConfigDetailstoEdit(string ID)
        {
            try
            {
                EmailConfig EmailConfigDetail = new Managers.ServiceManager().GetEditEmailConfig(ID);
                EmailConfigDetail.Password = new Managers.ServiceManager().Decryptdata(EmailConfigDetail.Password);
                return Json(new { Data = EmailConfigDetail, result = true });
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Already Exists", result = false });
            }

        }

       
        public JsonResult SaveConfig(EmailConfig param)
        {
            try
            {
                bool result = new Managers.ServiceManager().SaveEmailConfig(param);

                return Json(new { msg = "Record Saved", result = true });
                //  bool result = new Managers.RouteSecurityManager().SaveRouteSecurity(sm);
                //  return Json(new { msg = "Record Saved", result = true, EnvId = sm.EnvId });
            }
            catch (Exception e)
            {
                return Json(new { msg = "Record Already Exists", result = false });
            }

        }

    }
}