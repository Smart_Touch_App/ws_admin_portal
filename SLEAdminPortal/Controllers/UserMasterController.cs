﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Controllers
{
    public class UserMasterController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //UserMaster Index 
        public ActionResult UserMaster(string UserID="", string Names="", string DomainUserName="")
        {
            var envList = new Managers.DeviceEnvironmentMappingManager().GetEnvDdl();
            ViewData["Environment"] = new SelectList(envList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");

            UserModel models = new UserModel();
            models.Users = new Managers.UserMasterManager().GetUserList(UserID, Names, DomainUserName);
            return View("UserMaster", models);
        }

        //To Add the New User
        public ActionResult SaveUser(UserModel um)
        {
            try
            {
                UserModel models = new UserModel();
                models.Users = new Managers.UserMasterManager().SaveUser(um.NTUser, um.FirstName, um.LastName, um.DisplayName, um.Title, um.Domain, um.Email, um.Phone);
                //if(models.Users==null)
                //TempData["notice"] = "Successfully registered";
                //else
                // TempData["notice"] = "User Already Exists";
                return RedirectToAction("UserMaster");
            }
            catch (Exception ex)
            {
                // logger.Error("Error in UserMapController Index" + ex.Message);
                throw ex;
            }
        }

        //To Edit the Paticular User in Popup View
        public ActionResult EditUser(int App_user_id=0)
        {
            try
            {
                User models = new User();
                models = new Managers.UserMasterManager().GetUserForEdit(App_user_id);
                return PartialView("_EditUser", models);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //To Update The Particular User Popup View
        public ActionResult UpdateUser(User models)
        {
            try
            {
                models = new Managers.UserMasterManager().UpdateUser(models);
                return RedirectToAction("UserMaster");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //To Delete the Particular User 
        public ActionResult DeleteUser(int App_user_id=0)
        {
            try
            {
                UserModel model = new UserModel();
                model.Users = new Managers.UserMasterManager().DeleteUser(App_user_id);
                return RedirectToAction("UserMaster");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //To change the Active or Inactive
        public ActionResult ToggleActive(string Active, string App_User)
        {
            try
            {
                logger.Info("UserMasterController ToggleActive");
                new Managers.UserMasterManager().ToggleUpdate(Active, App_User);
                return Json(new { result = true });
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterController ToggleActive" + ex.Message);
                return Json(new { result = false });
            }
        }

        public JsonResult checkDuplicateAppUser(string App_User)
        {
            string result = new Managers.UserMasterManager().CheckForDuplicateUserID(App_User);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
      

        public JsonResult CheckDuplicateEmail(string Email)
        {
            string result = new Managers.UserMasterManager().CheckForDuplicateEmail(Email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
      

        public JsonResult GetEnvironmentByAppUserId(string AppUserId)
        {
            string result = new Managers.UserMasterManager().GetEnvironmentByUserID(AppUserId);
            return Json(new { result = true, msg = result });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveEnvironment(FormCollection collection)
        {
            string dev = collection["HdnAppUserId"];
            string env = collection["SelectEnvironment"];
            string oldEnv = collection["HdnEnvironment"];
            bool result = false;

            string[] arrOldEnv = oldEnv.Split(',');
            foreach (var i in arrOldEnv)
            {
                if (!string.IsNullOrEmpty(i))
                    result = new Managers.UserMasterManager().DeleteUserEnvironmentMapping(dev, Convert.ToInt32(i));
            }

            if (env != null)
            {
                string[] arrEnv = env.Split(',');
                foreach (var i in arrEnv)
                {
                    if (!string.IsNullOrEmpty(i))
                        result = new Managers.UserMasterManager().SaveUserEnvironmentMapping(Convert.ToInt32(i), dev);
                }
            }
            return RedirectToAction("UserMaster", "UserMaster");
        }

    }
}