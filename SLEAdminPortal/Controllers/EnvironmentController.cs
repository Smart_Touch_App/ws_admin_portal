﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEAdminPortal.Helpers;
using System.Data;
using log4net;
using System.Data.SqlClient;
using System.Configuration;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Controllers
{

    public class EnvironmentController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult EnvIndex(string EnvironmentMasterId)
        {
            //List Environment Details getting from Environment Manager:
            EnvironmentModels Envmodels = new EnvironmentModels();
            Envmodels.EnvironmentList = new Managers.EnvironmentManager().GetEnvironmnetList();
            return View(Envmodels);
        }

        //View Environment details getting from Environment Manager : 
        /*   public ActionResult Save(EnvironmentModels Env)
        {
            //EnvironmentModels Envmodels1 = new EnvironmentModels();
            bool result = new Managers.EnvironmentManager().SaveEnvironment(Env);
            return RedirectToAction("EnvIndex");
        }
        public ActionResult Delete(Environmentmaster Env)
        {
           EnvironmentModels Envmodels2 = new EnvironmentModels();
            Envmodels2.EnvironmentList = new Managers.EnvironmentManager().DeleteEnvironment(Env.EnvironmentMasterId);
            return RedirectToAction("EnvIndex");
        }*/
        public ActionResult ViewList(Int32 id)
        {
            Environmentmaster objenv = new Environmentmaster();
            objenv = new Managers.EnvironmentManager().GetById(id);
            return PartialView("_ViewEnvironment", objenv);
        }
        /*  public ActionResult Update(EnvironmentModels Env)
          {
              //EnvironmentModels Envmodels1 = new EnvironmentModels();
            //  bool result = new Managers.EnvironmentManager().EditEnvironment(Env);
              return RedirectToAction("EnvIndex");
          }*/
        /* public ActionResult ToggleActive(string State, string EnvironmentMasterId)
         {
             try
             {
                 logger.Info("EnvironmnetController ToggleActive Parameters: " + State + " " + EnvironmentMasterId);
                 new Managers.EnvironmentManager().UpdateRole(State, EnvironmentMasterId);
                 return RedirectToAction("EnvIndex");
             }
             catch (Exception ex)
             {
                 logger.Error("Error in EnvironmnetController ToggleActive" + ex.Message);
                 throw ex;
             }
         }*/


    }
}
