﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Controllers
{
    [Authorize]
    [SLEAdminPortal.FilterConfig.SessionExpire]
    public class DeviceEnvironmentMappingController :Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index()
        {
            try
            {
                if (TempData["ErrMsg"] != null)
                {
                    string msg = TempData["ErrMsg"].ToString().Replace('\'', ' ').Replace('\n', ' ').Replace('\r', ' ');

                    if (msg.Contains("Violation of PRIMARY KEY"))
                        msg = "Already Exists";

                    ViewBag.msg = msg;
                    TempData["ErrMsg"] = null;
                }
                //Get All Device Environment Mapping
                List<DeviceEnvironmentMappingModel> lst = new List<DeviceEnvironmentMappingModel>();
                lst = new Managers.DeviceEnvironmentMappingManager().GetDeviceEnvironmentList();
                return View(lst);
            }
            catch (Exception e)
            {
                ViewBag.msg = e.Message.ToString();
                return View();
            }
        }

        //Show Popup For Create
        public ActionResult Create()
        {
            try
            {
                DeviceEnvironmentMappingModel model = new DeviceEnvironmentMappingModel();

                //Load All Drop Down List
                var deviceLst = new Managers.DeviceEnvironmentMappingManager().GetDeviceDdl();
                ViewData["Device"] = new SelectList(deviceLst.OrderBy(x => x.Device_Id), "Device_Id", "FQDN");

                var envList = new Managers.DeviceEnvironmentMappingManager().GetEnvDdl();
                ViewData["Environment"] = new SelectList(envList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");

                return PartialView("_DevEnvMap", model);
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("Index");
            }
        }

        //Save Device Environment Mapping
        public ActionResult Save(DeviceEnvironmentMappingModel model)
        {
            try
            {
                bool result = new Managers.DeviceEnvironmentMappingManager().SaveDeviceEnvironmentMapping(model);
                TempData["ErrMsg"] = "Record Saved";
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("Index");
            }
        }

        // Show Popup For Edit
        public ActionResult Edit(string deviceid, decimal environmentid)
        {
            try
            {
                //Load All Drop Down List
                var deviceLst = new Managers.DeviceEnvironmentMappingManager().GetDeviceDdl();
                ViewData["Device"] = new SelectList(deviceLst.OrderBy(x => x.FQDN), "Device_Id", "FQDN");

                var envList = new Managers.DeviceEnvironmentMappingManager().GetEnvDdl();
                ViewData["Environment"] = new SelectList(envList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");

                // Load Single Record
                DeviceEnvironmentMappingModel lst = new DeviceEnvironmentMappingModel();
                lst = new Managers.DeviceEnvironmentMappingManager().GetByDeviceEnvironmentId(deviceid, environmentid);

                lst.EditDeviceId = deviceid;
                lst.EditEnvironmentMasterId = environmentid;

                return PartialView("_DevEnvMap", lst);
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("Index");
            }
        }

        //Update Device Environment Mapping
        public ActionResult Update(DeviceEnvironmentMappingModel model)
        {
            try
            {
                bool result = new Managers.DeviceEnvironmentMappingManager().EditDeviceEnvironmentMapping(model);
                TempData["ErrMsg"] = "Record Updated";
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("Index");
            }
        }

        //Delete Device Environment Mapping
        public ActionResult Delete(string deviceid, decimal environmentid)
        {
            try
            {
                bool result = new Managers.DeviceEnvironmentMappingManager().DeleteDeviceEnvironmentMapping(environmentid, deviceid);
                return Json(new { msg = "Record Deleted", result = true });
            }
            catch (Exception e)
            {
                return Json(new { msg = e.Message, result = true });
            }
        }

    }
}
