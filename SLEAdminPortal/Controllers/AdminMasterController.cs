﻿//************************************************************************************************
// Comment: Security Process Controller - Role Master, Securable Features, Role Features
// Created: Mar 20, 2016
// Author: TechUnison India (Velmani Karnan)
//*************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEAdminPortal.Models;
using System.Data;
using SLEAdminPortal.Managers;

namespace SLEAdminPortal.Controllers
{
    [Authorize]
    [SLEAdminPortal.FilterConfig.SessionExpire]
    public class AdminMasterController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult RoleMaster(string RoleID, string RoleNames, string RoleDescription)
        {
            try
            {
                AdminRoleModel models = new AdminRoleModel();
                if (!string.IsNullOrEmpty(RoleID) || !string.IsNullOrEmpty(RoleNames) || !string.IsNullOrEmpty(RoleDescription))
                {
                    models.AdminRoles = new Managers.AdminManager().GetSpecificRoleList(RoleID, RoleNames, RoleDescription);
                    return View("RoleMaster", models);
                }
                else
                {
                    models.AdminRoles = new Managers.AdminManager().GetRoleList();
                    return View("RoleMaster", models);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SaveNewRole(AdminRoleModel model)
        {
            try
            {
                if (model != null)
                {
                    LoginViewModel authenticatedUser = (LoginViewModel)(Session["user"]);
                    new Managers.AdminManager().InsertNewRole(model);
                }
                return RedirectToAction("RoleMaster");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult ToggleActive(string State, string RoleID)
        {
            try
            {
                logger.Info("DeviceController ToggleActive Parameters: " + State + " " + RoleID);
                new Managers.AdminManager().UpdateRole(State, RoleID);
                //return RedirectToAction("RoleMaster");
                return Json(new { result = true });
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController ToggleActive" + ex.Message);
                return Json(new { result = false });
            }
        }

        public ActionResult DeleteRole(string RoleID)
        {
            try
            {
                AdminRoleModel model = new AdminRoleModel();
                model.AdminRoles = new Managers.AdminManager().DeleteRole(RoleID);
                return RedirectToAction("RoleMaster");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SecurableFeaturesMaster(string FeatureID, string FeatureNames, string FeatureDescription)
        {
            try
            {
                SecurableFeaturesModel models = new SecurableFeaturesModel();
                models.SelectedOptionType = new Managers.SecurableFeatureManager().GetOptionType();
                models.SelectedToogleList = new Managers.SecurableFeatureManager().GetToggleList();
                if (!string.IsNullOrEmpty(FeatureID) || !string.IsNullOrEmpty(FeatureNames) || !string.IsNullOrEmpty(FeatureDescription))
                {
                    models.SecurableFeatures = new Managers.SecurableFeatureManager().GetSpecificSecurableFeatureList(FeatureID, FeatureNames, FeatureDescription);
                    return View("SecurableFeaturesMaster", models);
                }
                else
                {
                    models.SecurableFeatures = new Managers.SecurableFeatureManager().GetSecurableFeatureList();
                    return View("SecurableFeaturesMaster", models);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult SaveNewSecurableFeature(SecurableFeaturesModel model)
        {
            try
            {
                if (model != null)
                {
                    new Managers.SecurableFeatureManager().InsertNewSecurableFeature(model);
                }
                return RedirectToAction("SecurableFeaturesMaster");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult ToggleOptionType(string State, string FeatureID)
        {
            try
            {
                logger.Info("AdminMastereController ToggleOptionType Parameters: " + State + " " + FeatureID);
                new Managers.SecurableFeatureManager().UpdateFeatureType(State, FeatureID);
                return RedirectToAction("SecurableFeaturesMaster");
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController ToggleActive" + ex.Message);
                throw ex;
            }
        }

        public ActionResult ToggleOptionValue(string State, string FeatureID)
        {
            try
            {
                logger.Info("AdminController ToggleOptionValue Parameters: " + State + " " + FeatureID);
                new Managers.SecurableFeatureManager().UpdateFeatureValue(State, FeatureID);
                return RedirectToAction("SecurableFeaturesMaster");
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController ToggleActive" + ex.Message);
                throw ex;
            }
        }


        public ActionResult RoleFeaturesMaster(string RoleName, string FeatureName)
        {
            try
            {
                RoleFeaturesModel models = new RoleFeaturesModel();
                models.SecurableFeatureList = new Managers.RoleFeatureManager().GetFeatureSelectableList();
                models.RoleUserList = new Managers.RoleFeatureManager().GetRoleSelectableList();
                if (!string.IsNullOrEmpty(RoleName) || !string.IsNullOrEmpty(FeatureName))
                {
                    models.RoleFeatures = new Managers.RoleFeatureManager().GetSpecificRoleFeatureList(RoleName, FeatureName);
                    return View("RoleFeaturesMaster", models);
                }
                else
                {
                    models.RoleFeatures = new Managers.RoleFeatureManager().GetRoleFeatureList();
                    return View("RoleFeaturesMaster", models);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SaveNewRoleFeature(RoleFeaturesModel model)
        {
            try
            {
                if (model != null)
                {
                    new Managers.RoleFeatureManager().InsertNewRoleFeature(model);
                }
                return RedirectToAction("RoleFeaturesMaster");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ToggleActiveRoleFeature(string State, string RoleID, string FeatureID)
        {
            try
            {
                logger.Info("AdminController ToggleActiveRoleFeature Parameters: " + State + " " + RoleID + " " + FeatureID);
                new Managers.RoleFeatureManager().UpdateRoleFeature(State, RoleID, FeatureID);
                //return RedirectToAction("RoleFeaturesMaster");
                return Json(new { result = true });
            }
            catch (Exception ex)
            {
                logger.Error("Error in AdminController ToggleActive" + ex.Message);
                return Json(new { result = false });
            }
        }

        public JsonResult GetOptionTypeAndSettingValues(int featureID)
        {
            try
            {
                logger.Info("AdminController GetOptionTypeAndSettingValues Parameters: " + featureID);
                DataSet ds = new RoleFeatureManager().GetOptionType(featureID);
                return Json(new { optionType = Convert.ToString(ds.Tables[0].Rows[0]["OptionType"]), optionValue = Convert.ToString(ds.Tables[0].Rows[0]["OptionValue"]), defaultValue = Convert.ToString(ds.Tables[0].Rows[0]["DefaultValue"]) });
            }
            catch (Exception ex)
            {
                logger.Error("Error in AdminController ToggleActive" + ex.Message);
                return null;
            }
        }

        public JsonResult SaveRoleFeature(int roleId, int featureId, string settingValue)
        {
            try
            {
                char[] trimChar = { '|' };
                settingValue = settingValue.TrimStart(trimChar);
                logger.Info("AdminController SaveRoleFeature Parameters: " + roleId + " " + featureId + " " + settingValue);
                new Managers.RoleFeatureManager().SaveRoleFeature(roleId, featureId, settingValue);
                return Json(new { success = 1 });
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Violation of PRIMARY KEY constraint 'pk_RoleFeatures'"))
                {
                    return Json(new { success = 2 });
                }

                logger.Error("Error in AdminController SaveRoleFeature" + ex.Message);
                return Json(new { success = 0 });
            }
        }
        // Delete Role
        public JsonResult DeleteRoleFeature(int roleID, int featureID)
        {
            try
            {
                logger.Info("AdminController DeleteRoleFeature Parameters: " + roleID + " " + featureID);
                new Managers.RoleFeatureManager().DeleteRoleFeature(roleID, featureID);
                return Json(new { success = 1 });
            }
            catch (Exception ex)
            {
                logger.Error("Error in AdminController DeleteRoleFeature" + ex.Message);
                return Json(new { success = 0 });
            }
        }

        public JsonResult SaveRoleFeatureList(int roleID, int featureID, string settingValue)
        {
            try
            {
                logger.Info("AdminController SaveRoleFeatureList Parameters: " + roleID + " " + featureID + " " + settingValue);
                new Managers.RoleFeatureManager().SaveRoleFeatureList(roleID, featureID, settingValue);
                return Json(new { success = 1 });
            }
            catch (Exception ex)
            {
                logger.Error("Error in AdminController SaveRoleFeatureList" + ex.Message);
                return Json(new { success = 0 });
            }
        }
    }
}