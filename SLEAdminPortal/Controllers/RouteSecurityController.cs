﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Controllers
{
    [Authorize]
    [SLEAdminPortal.FilterConfig.SessionExpire]
    public class RouteSecurityController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult RouteSecurityIndex(int EnvId = 0, string RouteName = null, string UserName = null)
        {
            try
            {            
                if (TempData["ErrMsg"] != null)
                {
                    string msg = TempData["ErrMsg"].ToString().Replace('\'', ' ').Replace('\n', ' ').Replace('\r', ' ');
                    ViewBag.msg = msg;
                    TempData["ErrMsg"] = null;
                }

                //Get All Environment            
                    var EnvList = new Managers.RouteSecurityManager().GetEnvDdl();
                    if (EnvId > 0)
                    {
                        ViewBag.SelectedEnv = EnvId;                       
                    }                 
                        ViewData["Environment"] = new SelectList(EnvList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");               
                    if (HttpContext.Request.IsAjaxRequest())
                    {
                        //Get All Route Security
                        RouteSecurityModels lst = new RouteSecurityModels();
                        lst.Route = new Managers.RouteSecurityManager().GetRouteSecurityList(EnvId, RouteName, UserName);

                        return PartialView("_SearchRouteSecurity", lst);
                    }
               
                    return View("RouteSecurityIndex",EnvId);
                
            }
            catch (Exception e)
            {
                ViewBag.msg = e.Message.ToString();
                return View();
            }
        }

        //Show Popup For Create
        public ActionResult Create(int EnvId = 0, string EnvName = "")
        {
            try
            {
                RouteSecurity rs = new RouteSecurity();

                //Load All Drop Down List
                var UserList = new Managers.RouteSecurityManager().GetUserDdl();
                ViewData["User"] = new SelectList(UserList.OrderBy(x => x.App_User), "App_user_id", "App_User");

                var RoleList = new Managers.RouteSecurityManager().GetRoleDdl();
                ViewData["Role"] = new SelectList(RoleList.OrderBy(x => x.RoleName), "RoleID", "RoleName");

                var RouteList = new Managers.RouteSecurityManager().GetRouteDdl(EnvId);
                ViewData["Route"] = new SelectList(RouteList.OrderBy(x => x.RouteName), "RouteMasterID", "RouteName");

                rs.EffectiveFrom = DateTime.Now;
                rs.EffectiveTo = DateTime.Now;

                rs.EnvName = EnvName;
                rs.EnvId = EnvId;

                return PartialView("_RouteSecurity", rs);
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("RouteSecurityIndex");
            }
        }

        public ActionResult UpdateDiv(int EnvId = 0, string RouteName = null, string UserName = null)
        {
            RouteSecurityModels rs = new RouteSecurityModels();
            rs.Route = new Managers.RouteSecurityManager().GetRouteSecurityList(EnvId, RouteName, UserName);
            return PartialView("_SearchRouteSecurity", rs);
        }



        //Save the Route Security
        public JsonResult Save(RouteSecurity rs)
        {
            try
            {
                bool result = new Managers.RouteSecurityManager().SaveRouteSecurity(rs);           
                return Json(new { msg = "Record Saved", result = true,EnvId=rs.EnvId });
            }
            catch (Exception e)
            {          
                return Json(new{msg="Record Already Exists",result=false,EnvId = rs.EnvId});
            }
        }        

        // Show Popup For Edit
        public ActionResult Edit(int route, int user, int role, int EnvId, string EnvName)
        {
            try
            {
                //Load All Drop Down List
                var UserList = new Managers.RouteSecurityManager().GetUserDdl();
                ViewData["User"] = new SelectList(UserList.OrderBy(x => x.App_User), "App_user_id", "App_User");

                var RoleList = new Managers.RouteSecurityManager().GetRoleDdl();
                ViewData["Role"] = new SelectList(RoleList.OrderBy(x => x.RoleName), "RoleID", "RoleName");

                var RouteList = new Managers.RouteSecurityManager().GetRouteDdl(EnvId);
                ViewData["Route"] = new SelectList(RouteList.OrderBy(x => x.RouteName), "RouteMasterID", "RouteName");

                // Load Single Record
                RouteSecurity lst = new RouteSecurity();
                lst = new Managers.RouteSecurityManager().GetByRouteSecurityId(route, role, user, EnvId);

                lst.EditRoleId = role;
                lst.EditRouteId = route;
                lst.EditUserId = user;
                lst.EnvId = EnvId;
                lst.EnvName = EnvName;           
                return PartialView("_RouteSecurity", lst);
            }
            catch (Exception e)
            {
                TempData["ErrMsg"] = e.Message.ToString();
                return RedirectToAction("RouteSecurityIndex");
            }
        }

        //Update the Route Security
        public JsonResult Update(RouteSecurity rs)
        {
            try
            {
                bool result = new Managers.RouteSecurityManager().EditRouteSecurity(rs);           
                return Json(new { msg = "Record Updated", result = true, EnvId = rs.EnvId });
            }
            catch (Exception e)
            {
               // TempData["ErrMsg"] = e.Message.ToString();
                return Json(new{msg="Record Already Exists",result=false,EnvId = rs.EnvId });
            }
        }

        //Delete the Route Security
        public ActionResult Delete(int route, int user, int role, int EnvId)
        {
            try
            {
                bool result = new Managers.RouteSecurityManager().DeleteRouteSecurity(route, role, user, EnvId);
                return Json(new { msg = "Record Deleted", result = true });
            }
            catch (Exception e)
            {
                return Json(new { msg = e.Message, result = false });
            }
        }

    }
}
