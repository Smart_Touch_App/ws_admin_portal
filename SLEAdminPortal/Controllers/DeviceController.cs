﻿using System;
using System.Linq;
using System.Web.Mvc;
using SLEAdminPortal.Models;
using System.Text;
using System.Web;

namespace SLEAdminPortal.Controllers
{
    public class DeviceController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index(string FQDNSearch = "", string MachineName ="", string Environment = "")
        {
            if (TempData["message"] != null)
            {
                ViewBag.msg = TempData["message"];
                TempData["message"] = null;
            }
            try
            {
                DeviceModels models = new DeviceModels();
                models.Devices = new Managers.DeviceManager().GetDeviceList();

                var envList = new Managers.DeviceEnvironmentMappingManager().GetEnvDdl();
                ViewData["Environment"] = new SelectList(envList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");

                if (!string.IsNullOrEmpty(FQDNSearch) && !string.IsNullOrEmpty(MachineName) && !string.IsNullOrEmpty(Environment))
                {
                    models.Devices = new Managers.DeviceManager().GetSpecificDeviceList(FQDNSearch.Replace(" ", ""), MachineName.Replace(" ", ""), Environment.Replace(" ", ""));
                    return View("Index", models);
                }
                else if (!string.IsNullOrEmpty(FQDNSearch) || !string.IsNullOrEmpty(MachineName) || !string.IsNullOrEmpty(Environment))
                {
                    models.Devices = new Managers.DeviceManager().GetSpecificDeviceList(FQDNSearch.Replace(" ", ""), MachineName.Replace(" ", ""), Environment.Replace(" ", ""));
                    return View("Index", models);
                }
                else
                {
                    return View("Index", models);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveDevice(FormCollection collection)
        {
            try
            {
                string FQDN = collection["FQDN"];
                //string Environment = collection["SelectEnvironment"];
                string Environment = null;
                string Manufacture = collection["Manufacturer"];
                string Status = collection["RouteStatus"];
                string Model = collection["SaveModel"];
                string MachineName = collection["SaveMachineName"];
                string IsNew = collection["IsNew"];

                if (IsNew == "Yes")
                {
                    Int32? value = 0;
                    Managers.DeviceManager manager = new Managers.DeviceManager();
                    string MachineID = manager.AddDeviceInformation(FQDN, MachineName, Environment, Status, Manufacture, Model);
                    if (Convert.ToInt32(MachineID) > value)
                    {
                        return RedirectToAction("Index", "Device");
                    }
                }
                else
                {
                    string oldMachineID = Session["MachineID"].ToString();
                    Managers.DeviceManager manager = new Managers.DeviceManager();
                    string MachineID = manager.UpdateDeviceInfo(oldMachineID, FQDN, MachineName, Manufacture, Model);
                    Session["MachineID"] = null;
                    return RedirectToAction("Index", "Device");
                }
            }
            catch(Exception ex)
            {
                TempData["message"] = ex.Message;
            }         
            return RedirectToAction("Index", "Device");
        }
        public ActionResult EditDeviceInfo(string MachineID = "")
        {
            Session["MachineID"] = MachineID;
            var result = new Managers.DeviceManager().DeviceInfo(MachineID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeviceDeregister(string MachineID="", string Route="")
        {
            DeviceModels models = new DeviceModels();
            models.Devices = new Managers.DeviceManager().DeviceDeregister(MachineID, Route);
            //models.Devices = new Managers.DeviceManager().GetDeviceList();

            //var envList = new Managers.DeviceEnvironmentMappingManager().GetEnvDdl();
            //ViewData["Environment"] = new SelectList(envList.OrderBy(x => x.EnvName), "EnvironmentMasterId", "EnvName");

            return RedirectToAction("Index", "Device");
        }
        public ActionResult ChangeActiveFlag(string MachineID="", string Active="")
        {
            if (Active == "0")
            {
                string Devices = new Managers.DeviceManager().ChangeActiveFlag(MachineID, Active);
                //return RedirectToAction("Index", "Device");
                return Json(new { result = true });
            }
            else
            {
                string Devices = new Managers.DeviceManager().ChangeActiveFlag(MachineID, Active);
                //return RedirectToAction("Index", "Device");
                return Json(new { result = true });
            }

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveEnvironment(FormCollection collection)
        {
            string dev = collection["HdnDeviceId"];
            string env = collection["SelectEnvironment"];
            string oldEnv = collection["HdnEnvironment"];
            bool result = false;

            string[] arrOldEnv = oldEnv.Split(',');
            foreach (var i in arrOldEnv)
            {
                if (!string.IsNullOrEmpty(i))
                    result = new Managers.DeviceManager().DeleteDeviceEnvironmentMapping(dev, Convert.ToInt32(i));
            }

            if (!string.IsNullOrEmpty(env))
            {
                string[] arrEnv = env.Split(',');
                foreach (var i in arrEnv)
                {
                    if (!string.IsNullOrEmpty(i))
                        result = new Managers.DeviceManager().SaveDeviceEnvironmentMapping(Convert.ToInt32(i), dev);
                }
            }
            return RedirectToAction("Index", "Device");
        }

        public JsonResult GetEnvironmentByDeviceId(string DeviceId="")
        {
            string result = new Managers.DeviceManager().GetEnvironmentByDeviceId(DeviceId);
            return Json(new { result = true, msg = result });
        }

    }

    public static class HtmlExtensions
    {
        public static MvcHtmlString Nl2Br(this HtmlHelper htmlHelper, string text)
        {
            if (string.IsNullOrEmpty(text))
                return MvcHtmlString.Create(text);
            else
            {
                StringBuilder builder = new StringBuilder();
                string[] lines = text.Split(',');
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i > 0)
                        builder.Append("<br/>\n");
                    builder.Append(HttpUtility.HtmlEncode(lines[i]));
                }
                return MvcHtmlString.Create(builder.ToString());
            }
        }
    }

}