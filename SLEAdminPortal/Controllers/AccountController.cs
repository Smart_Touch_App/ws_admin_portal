﻿using System;
using System.Web.Mvc;
using SLEAdminPortal.Models;
using System.Web;
namespace SLEAdminPortal.Controllers
{
    public class AccountController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Account
        public ActionResult Index()
       {
            Managers.UserManager userManager = new Managers.UserManager();
            LoginViewModel authenticatedUser = userManager.LoginUser(Server.HtmlEncode(User.Identity.Name));
            Session["DomainName"] = User.Identity.Name;
            if (authenticatedUser != null)
            {
                Session["user"] = authenticatedUser;
                return RedirectToAction("Index", "Device");
            }
            else
            {
                LogOff();
                return RedirectToAction("Index", "Account");
            }
        }

        public ActionResult Logout()
        {
            try
            {
                logger.Info("AccountController Logout");
                LogOff();
                return RedirectToAction("Index", "Account");
            }
            catch (Exception ex)
            {
                logger.Error("Error in AccountController Logout" + ex.Message);
                throw ex;
            }
        }

        private void LogOff()
        {
            HttpCookie cookie = base.Request.Cookies["TSWA-Last-User"];

            if (base.User.Identity.IsAuthenticated == false || cookie == null || StringComparer.OrdinalIgnoreCase.Equals(base.User.Identity.Name, cookie.Value))
            {

                string name = string.Empty;
                if (base.Request.IsAuthenticated)
                {
                    name = this.User.Identity.Name;
                }

                cookie = new HttpCookie("TSWA-Last-User", name);
                base.Response.Cookies.Set(cookie);

                base.Response.AppendHeader("Connection", "close");
                base.Response.StatusCode = 0x191;
                base.Response.Clear();
                //should probably do a redirect here to the unauthorized/failed login page
                //if you know how to do this, please tap it on the comments below
                base.Response.Write("Access denied");
                base.Response.End();
            }

            cookie = new HttpCookie("TSWA-Last-User", string.Empty)
            {
                Expires = DateTime.Now.AddYears(-5)
            };

        }
    }
}