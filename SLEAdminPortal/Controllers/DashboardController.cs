﻿using System;
using System.Linq;
using System.Web.Mvc;
using SLEAdminPortal.Models;
namespace SLEAdminPortal.Controllers
{
    [Authorize]
    [SLEAdminPortal.FilterConfig.SessionExpire]
    public class DashboardController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index(string DeviceID, string MachineName, string Environment)
        {
            try
            {
                DeviceModels models = new DeviceModels();
                models.Devices = new Managers.RouteManager().GetDeviceList();
                models.Environment = new Managers.RouteManager().GetEnvironment();
                if (!string.IsNullOrEmpty(DeviceID) && !string.IsNullOrEmpty(MachineName) && !string.IsNullOrEmpty(Environment))
                {
                    models.Devices = new Managers.RouteManager().GetSpecificDeviceList(DeviceID.Replace(" ", ""), MachineName.Replace(" ", ""), Environment.Replace(" ", ""));
                    return View("Index", models);
                }
                else if (!string.IsNullOrEmpty(DeviceID) || !string.IsNullOrEmpty(MachineName) || !string.IsNullOrEmpty(Environment))
                {
                    models.Devices = new Managers.RouteManager().GetSpecificDeviceList(DeviceID.Replace(" ", ""), MachineName.Replace(" ", ""), Environment.Replace(" ", ""));
                    return View("Index", models);
                }
                else
                {
                    return View("Index", models);
                }
                //return View(models);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveDevice(FormCollection collection)
        {
            string MachineID = collection["MachineID"];
            string Environment = collection["SelectEnvironment"];
            string MachineName = collection["Manufacturer"];
            string Status = collection["RouteStatus"];
            if(!string.IsNullOrEmpty(Environment))
            {
                int count = Environment.Split(',').Count();
                string[] arr = Environment.Split(',');
                Int32? value = 0;
                Managers.UserManager userManager = new Managers.UserManager();
                MachineID = userManager.AddDeviceInformation(MachineID, MachineName, Environment, Status);
                if (Convert.ToInt32(MachineID) > value)
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            else
            {
                string oldMachineID = Session["MachineID"].ToString();
                Managers.UserManager userManager = new Managers.UserManager();
                MachineID = userManager.UpdateDeviceInfo(oldMachineID,MachineName);
                return RedirectToAction("Index", "Dashboard");
            }
            return RedirectToAction("Index", "Dashboard");
        }
        public ActionResult EditDeviceInfo(string MachineID)
        {
            Session["MachineID"] = MachineID;
            var result = new Managers.RouteManager().DeviceInfo(MachineID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeviceDeregister(string MachineID,string Route)
        {
            DeviceModels models = new DeviceModels();
            models.Devices = new Managers.RouteManager().DeviceDeregister(MachineID, Route);
            models.Devices = new Managers.RouteManager().GetDeviceList();
            models.Environment = new Managers.RouteManager().GetEnvironment();
            return View("Index", models);
        }
        public ActionResult ChangeActiveFlag(string MachineID,string Active)
        {
            if(Active == "0")
            {
                string Devices = new Managers.RouteManager().ChangeActiveFlag(MachineID,Active);
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                string Devices = new Managers.RouteManager().ChangeActiveFlag(MachineID,Active);
                return RedirectToAction("Index", "Dashboard");
            }
            
        }
    }
    
}