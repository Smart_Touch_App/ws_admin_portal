﻿using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
        public class SessionExpireAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                HttpContext ctx = HttpContext.Current;

                // check  sessions here
                if (HttpContext.Current.Session["user"] == null)
                {
                    filterContext.Result = new RedirectResult("~/Account/Index");
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
        }
    }
}
