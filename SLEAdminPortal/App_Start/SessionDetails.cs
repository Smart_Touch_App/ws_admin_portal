﻿
namespace SLEAdminPortal.App_Start
{
    public class SessionDetails
    {
        public string UserName
        {
            get;
            set;
        }
        public string Password { get; set; }
    }
}