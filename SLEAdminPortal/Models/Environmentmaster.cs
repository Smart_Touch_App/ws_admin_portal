﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace SLEAdminPortal.Models
{
    public class Environmentmaster
    {
        public int EnvironmentMasterId { get; set; }
        [Required]
        public string EnvName { get; set; }
        [Required]
        public string EnvDescription { get; set; }
        public string IsActive { get; set; }
        [Required]
        public string ReleaseLevel { get; set; }
        public string MLServerAddress { get; set; }
        public string JDEWSAddress { get; set; }
        public string REFDBAddress { get; set; }
        public string ENVDBAddress { get; set; }
        public string CDBAddress { get; set; }
        public string CDBConKey { get; set; }
        public string IISAddress { get; set; }
        public string AppInstallerLocation { get; set; }
        public string RDBScriptLocation { get; set; }
        public string TaxLibLocation { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }
        [Required]
        public string PortalConnectionString { get; set; }



    }

}

