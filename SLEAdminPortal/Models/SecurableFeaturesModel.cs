﻿//************************************************************************************************
// Comment: Securable Feature Models
// Created: Mar 20, 2016
// Author: TechUnison India (Velmani Karnan)
//*************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal.Models
{
    public class SecurableFeaturesModel : SecurableFeature
    {
        public List<SecurableFeature> SecurableFeatures;
        public IEnumerable<SelectListItem> SelectedOptionType;
        public IEnumerable<SelectListItem> SelectedToogleList;
    }
}