﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal.Models
{
    public class RoleFeaturesModel : RoleFeature
    {
        public List<RoleFeature> RoleFeatures;
        public IEnumerable<SelectListItem> RoleUserList;
        public string SelectedRoleUser { get; set; }
        public IEnumerable<SelectListItem> SecurableFeatureList;
        public string SelectedFeature { get; set; }
    }
}