﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SLEAdminPortal.Models
{
    public class DeviceEnvironmentMappingModel
    {
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public decimal EnvironmentMasterId { get; set; }
        public string EnvName { get; set; }

        public decimal EditEnvironmentMasterId { get; set; }
        public string EditDeviceId { get; set; }
    }

    public class DeviceDdl
    {
        public string Device_Id { get; set; }
        public string FQDN { get; set; }
    }

    public class EnvDdl
    {
        public decimal EnvironmentMasterId { get; set; }
        public string EnvName { get; set; }
    }

}