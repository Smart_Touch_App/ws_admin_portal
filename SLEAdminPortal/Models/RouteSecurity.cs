﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SLEAdminPortal.Models
{
    public class RouteSecurity
    {
        [Required]
        public decimal RouteMasterID { get; set; }
        public string RouteName { get; set; }

        [Required]
        public int App_user_id { get; set; }
        public string App_User { get; set; }

        [Required]
        public int RoleID { get; set; }
        public string RoleName { get; set; }

        [Required]
        public DateTime EffectiveFrom { get; set; }
        [Required]
        public DateTime EffectiveTo { get; set; }

        public int EditUserId { get; set; }
        public int EditRoleId { get; set; }
        public int EditRouteId { get; set; }

        public int EnvId { get; set; }
        public string EnvName { get; set; }
    }

    public class UserDdl
    {
        public int App_user_id { get; set; }
        public string App_User { get; set; }
    }

    public class RoleDdl
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class RouteDdl
    {
        public int RouteMasterID { get; set; }
        public string RouteName { get; set; }
    }

    public class EnvironmentDdl
    {
        public int EnvironmentMasterId { get; set; }
        public string EnvName { get; set; }
    }

}