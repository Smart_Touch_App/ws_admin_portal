﻿using System;
using System.ComponentModel.DataAnnotations;
namespace SLEAdminPortal.Models
{
    public class Device
    {
        public string DeviceID { get; set; }
        public string Manufacturer { get; set; }
        public string model { get; set; }
        public string MachineID { get; set; }
        [Required]
        public string ComptuerName { get; set; }
        [Required]
        public string FQDN { get; set; }
        public string EnvironmentId { get; set; }
        public string Environment { get; set; }
        public int App_user_id { get; set; }
        public string Route { get; set; }
        public string Status { get; set; }
        public string VersionNumber { get; set; }
        public string ReleasedDate { get; set; }
        public Int32 Active { get; set; }
    }

    public class RemoteDbNew
    {
        public string RouteID { get; set; }
        public string ActiveYN { get; set; }
    }

    public class DeviceStatusRoute
    {
        public string RouteStatus { get; set; }
        public string RouteName { get; set; }
        public string AppVersionNumber { get; set; }
        public string AppReleasedDate { get; set; }
    }

}