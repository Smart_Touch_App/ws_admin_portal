﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal.Models
{
    public class DeviceModels
    {
        public List<Device> Devices;
        public IEnumerable<SelectListItem> Environment;
    }
}