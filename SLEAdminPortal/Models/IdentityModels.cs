﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace SLEAdminPortal.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
       
    }
    [Flags]
    public enum Role {
            RSR = 1,
            RSM = 2,     
            DSM = 3,            
            ITADMIN = 4
     }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}