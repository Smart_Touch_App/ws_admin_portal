﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEAdminPortal.Models
{
    public class ServiceModel :Service 
    {
       
        public IEnumerable<SelectListItem> RouteList;
        public IEnumerable<SelectListItem> RouteList1;
        public IEnumerable<SelectListItem> JobsList;
        public IEnumerable<SelectListItem> Dependency;
        public IEnumerable<SelectListItem> DailyFrequentsList;
        public IEnumerable<SelectListItem> WeekDaysList;
        public List<JobDetails> JobDetails;
        public List<ModuleSequence> ModuleSequences;
        public List<EmailConfig> EmailConfigDetails;
        public bool IsServiceRunning;
        public List<Service> Services;
        public List<ImmediateService> ImmediateServices;
        public List<ServiceSubModule> ServiceSubModules;
        public List<ServiceHistory> ServiceHistorys;
    }
}