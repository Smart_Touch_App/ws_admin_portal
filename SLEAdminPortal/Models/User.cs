﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SLEAdminPortal.Models
{
    public class User
    {
        public int App_user_id { get; set; }
        [Required]
        public string NTUser { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string DisplayName { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Domain { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AppPassword { get; set; }
        public int Active { get; set; }
        public string EnvironmentId { get; set; }
        public string Environment { get; set; }
        

    }
}