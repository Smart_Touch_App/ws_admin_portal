﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Management;

namespace SLEAdminPortal.Models
{
    public class Service : JobDetails
    {
        public int ScheduleID { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public bool IsServiceRunning { get; set; }
        public string JDEInterfaceID { get; set; }
        public string ErrorStatus { get; set; }
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
        public int EnvironmentID { get; set; }
        public string EnvironmentName { get; set; }
        public string ServiceName { get; set; }
        public bool ServiceStopStart { get; set; }
        public string ScheduleInterval { get; set; }
        public string ScheduleFrequency { get; set; }
        public string OnceorFrequency { get; set; }
        public string ScheduleStatus { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime EffectiveTo { get; set; }
        public DateTime NextRunTime { get; set; }
        public int IsWaiting { get; set; }
        public int IsExecuting { get; set; }
        public bool IsRead { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }

        public string EffectiveFromString { get; set; }
        public string EffectiveToString { get; set; }
    }

    public class ImmediateService
    {
        public int ScheduleID { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string EnvironmentName { get; set; }
        public string ServiceName { get; set; }
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
        public string ScheduleStatus { get; set; }
        public bool IsRead { get; set; }
        public bool IsActive { get; set; }
        public bool IsImmediate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }

    public class JobDetails : ModuleSequence
    {
       
        public int JobID { get; set; }
        public string JobName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsActive { get; set; }
    }

    public class ModuleSequence : EmailConfig
    {
        public int ID { get; set; }
        public int JobID { get; set; }
        public int SequenceNo { get; set; }
        public string ModuleName { get; set; }
        public string Dependency { get; set; }
        public string EmailNotification { get; set; }
        public string EmailAddress { get; set; }
        public string Status { get; set; }
    }


    public class EmailConfig
    {
        public int ID { get; set; }
        public string GroupEmailName { get; set; }
        public string FromRecipients { get; set; }
        public string Password { get; set; }
        public string ToRecipients { get; set; }
        public string CcRecipients { get; set; }
        public string BccRecipients { get; set; }
        public DateTime StartedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }

    public class ModuleDdl
    {
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
    }

    public class ServiceHistory
    {
    
        public int HistoryID { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string ModuleName { get; set; }
        public string EnvironmentName { get; set; }
        public DateTime LastRunDateTime { get; set; }
        public string Status { get; set; }
        public DateTime LastUpdateDateTime { get; set; }

    }

    public class ServiceSubModule
    {
        public int ID { get; set; }
        public string SUBModuleName { get; set; }
        public string JDEInterfaceID { get; set; }
        public string ErrorStatus { get; set; }
        public string ScheduleStatus { get; set; }
        public string ScheduleInterval { get; set; }
        public DateTime StartedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }

    public class EmailGroupDdl
    {
        public int ID { get; set; }
        public string GroupEmailName { get; set; }
    }


    





}