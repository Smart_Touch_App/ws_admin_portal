﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SLEAdminPortal.Models
{
    public class AdminRole
    {
        public string RoleID { get; set; }
        [Required(ErrorMessage = "Please Enter Role Name")]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int Active { get; set; }
        public int Weightage { get; set; }
    }
}