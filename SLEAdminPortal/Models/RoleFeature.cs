﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEAdminPortal.Models
{
    public class RoleFeature
    {
        public string RoleID { get; set; }
        public string FeatureID { get; set; }
        public string RoleName { get; set; }
        public string FeatureName { get; set; }
        public string SettingValue { get; set; }
        public string OptionValue { get; set; }
    }
}