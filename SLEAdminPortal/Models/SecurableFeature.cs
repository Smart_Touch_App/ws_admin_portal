﻿//************************************************************************************************
// Comment: Securable Feature Properties
// Created: Mar 20, 2016
// Author: TechUnison India (Velmani Karnan)
//*************************************************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SLEAdminPortal.Models
{
    public class SecurableFeature
    {
        public string FeatureID { get; set; }
        [Required(ErrorMessage="Please Enter Feature Name")]
        [Display(Name = "Feature Name")]
        public string FeatureName { get; set; }
        public string FeatureDescription { get; set; }
        public string OptionType { get; set; }
        public string OptionValue { get; set; }
        public string FromAppRelease { get; set; }
        public string ToAppRelease { get; set; }
        public string FromPortalRelease { get; set; }
        public string ToPortalRelease { get; set; }
        public string DefaultValue { get; set; }
        public string SelectedToggleValue { get; set; }
        public string SelectedOption { get; set; }
    }
}