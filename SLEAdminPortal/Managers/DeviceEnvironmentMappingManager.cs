﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Web;
using SLEAdminPortal.Models;
using System.Web.Mvc;
using System.Data;
using log4net;
using System.Data.SqlClient;
using System.Configuration;

namespace SLEAdminPortal.Managers
{
    public class DeviceEnvironmentMappingManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Get Device Environment Mapping List
        public List<DeviceEnvironmentMappingModel> GetDeviceEnvironmentList()
        {
            try
            {
                logger.Info("Device Environment Mapping List");
                string query = string.Empty;

                query = " select a.DeviceId, b.EnvironmentMasterId, b.EnvName from BUSDTA.Device_Env_Map a, BUSDTA.Environment_Master b where a.EnvironmentMasterId=b.EnvironmentMasterId ";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<DeviceEnvironmentMappingModel> lst = new List<DeviceEnvironmentMappingModel>();
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<DeviceEnvironmentMappingModel>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Device Environment List" + ex.Message);
                throw ex;
            }
        }

        //Get Single Device Environment Mapping
        public DeviceEnvironmentMappingModel GetByDeviceEnvironmentId(string deviceid, decimal environmentid)
        {
            try
            {
                logger.Info("Device Environment Mapping GetByDeviceEnvironmentId");
                string query = string.Empty;
                query = " select a.DeviceId, a.EnvironmentMasterId, b.EnvName from BUSDTA.Device_Env_Map a, BUSDTA.Environment_Master b where a.EnvironmentMasterId=b.EnvironmentMasterId and a.EnvironmentMasterId=" + environmentid + " and a.DeviceId='" + deviceid + "'";
                
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                DeviceEnvironmentMappingModel lst = new DeviceEnvironmentMappingModel();
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntity<DeviceEnvironmentMappingModel>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Device Environment Mapping GetByDeviceEnvironmentId" + ex.Message);
                throw ex;
            }
        }

        //Save Device Environment Mapping
        public bool SaveDeviceEnvironmentMapping(DeviceEnvironmentMappingModel obj)
        {
            try
            {
                logger.Info("Device Environment Mapping Save");
                string query = string.Empty;

                query = " Insert into BUSDTA.Device_Env_Map VALUES("+obj.EnvironmentMasterId+",'"+obj.DeviceId+"',1,GETDATE(),null,null)";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment Mapping Save" + ex.Message);
                throw ex;
            }
        }

        //Edit Device Environment Mapping
        public bool EditDeviceEnvironmentMapping(DeviceEnvironmentMappingModel obj)
        {
            try
            {
                logger.Info("Device Environment Mapping Edit");
                string query = string.Empty;

                query = " update BUSDTA.Device_Env_Map set EnvironmentMasterId =" + obj.EnvironmentMasterId + ",DeviceId='" + obj.DeviceId + "',[UpdatedBy]=1 ,[last_modified]= GETDATE() where [EnvironmentMasterId]=" + obj.EditEnvironmentMasterId + " and [DeviceId]='" + obj.EditDeviceId +"'";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }

            catch (Exception ex)
            {
                logger.Error("Error in Device Environment Mapping Edit" + ex.Message);
                throw ex;
            }
        }

        //Delete Device Environment Mapping
        public bool DeleteDeviceEnvironmentMapping(decimal EnvironmentMasterId, string DeviceId)
        {
            try
            {
                logger.Info("DeviceEnvironmentMapping Delete");
                string query = string.Empty;

                query = "delete from BUSDTA.Device_Env_Map where EnvironmentMasterId=" + EnvironmentMasterId + " and DeviceId='" + DeviceId + "'";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceEnvironmentMapping Delete" + ex.Message);
                throw ex;
            }
        }
               
        //Environment DropDown List
        public List<EnvDdl> GetEnvDdl()
        {
            try
            {
                logger.Info("Environment DropDownList");
                string query = string.Empty;
                query = "select [EnvironmentMasterId],[EnvName] from BUSDTA.Environment_Master";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<EnvDdl> lst = new List<EnvDdl>();
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<EnvDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment DropDownList" + ex.Message);
                throw ex;
            }
        }

        // Device Drop Down List
        public List<DeviceDdl> GetDeviceDdl()
        {
            try
            {
                logger.Info("Device DropDownList");
                string query = string.Empty;
                query = "select [Device_Id], FQDN from BUSDTA.MachineMaster";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<DeviceDdl> lst = new List<DeviceDdl>();
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<DeviceDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Device DropDownList" + ex.Message);
                throw ex;
            }
        }
        
    }
}