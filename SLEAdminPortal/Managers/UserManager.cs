﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Data;
using SLEAdminPortal.Models;
using System.Web.Mvc;
namespace SLEAdminPortal.Managers
{
    public class UserManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public LoginViewModel LoginUser(string userID)
        {
            try
            {
                logger.Info("UserManager LoginUser parameters: " + userID);
                string query = string.Empty;
                query = "select 'Administrator' as UserName,'Administrator' as DisplayName, 0 as ID, 'ITADMIN' as Role from Admin_Master where Active = 1 and DomainUser = '{0}'";
                query = string.Format(query, userID);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);
                return dsDevices.HasData() ? dsDevices.GetEntityList<LoginViewModel>().First<LoginViewModel>() : null;
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserManager LoginUser" + ex.Message);
                throw ex;
            }
        }

        public string AddDeviceInformation(string MachineID, string MachineName, string Environment, string Status)
        {
            string query = string.Empty, query1 = string.Empty, query2 = string.Empty;
            int result = -1, result2 = -1;
            try
            {

                logger.Info("UserManager AddDeviceInformation parameters: " + MachineName + " " + MachineID + " " + Environment);
                query = "select COUNT(Device_ID) from BUSDTA.Device_Master where Device_ID = '{0}'";
                query = string.Format(query, MachineID.Trim());
                result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));

                if (result == 0)
                {
                    query = "INSERT INTO BUSDTA.Device_Master(Device_Id,DeviceName,Active,manufacturer,model,last_modified) VALUES ('{0}','{1}',1,'TechUnison','GETAC',GetDate()) ";
                    query = string.Format(query, MachineID, MachineName);
                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");
                    query1 = " SELECT EnvironmentMasterId FROM BUSDTA.Environment_Master WHERE EnvName ='" + Environment + "' ";
                    int result1 = Convert.ToInt32(DbHelper.ExecuteScalar(query1, null, "SystemDB"));
                    query2 = " INSERT INTO BUSDTA.Device_Environment_Map VALUES(" + result1 + ",'" + MachineID + "',1,GETDATE(),1,GETDATE())";
                    result2 = DbHelper.ExecuteNonQuery(query2, null, false, "SystemDB");
                }

                if (result > 0)
                {
                    query = "select COUNT(Device_ID) from BUSDTA.Device_Master where Device_ID = '{0}'";
                    query = string.Format(query, MachineID.Trim());
                    MachineID = DbHelper.ExecuteScalar(query, null, "SystemDB").ToString();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserManager AddDeviceInformation" + ex.Message);
                throw ex;
            }

            return MachineID;
        }

        public string UpdateDeviceInfo(string OldMachineID, string MachineName)
        {
            string query = string.Empty, query1 = string.Empty, UpdateDeviceInfo = string.Empty;
            int result = -1;
            query = "select COUNT(Device_ID) from BUSDTA.Device_Master where Device_ID = '" + OldMachineID + "'";
            result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));

            if (result > 0)
            {
                query = " Update BUSDTA.Device_Master SET DeviceName='" + MachineName + "' where Device_ID='" + OldMachineID + "'";
                UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
            }
            return UpdateDeviceInfo;
        }

        //public string AddApplciationUser(string displayName, string loginID, string password)
        //{
        //    string query = string.Empty;
        //    string userID = string.Empty;
        //    int result = -1;
        //    try
        //    {
        //        logger.Info("UserManager AddApplciationUser parameters: " + displayName + " " + loginID + " " + password);
        //        query = "select count(app_user) from busdta.synUserMaster where app_user='{0}'";
        //        query = string.Format(query, loginID.Trim());
        //        result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
        //        if (result == 0)
        //        {
        //            query = "insert into busdta.synUserMaster(app_user,appPassword,Name,DomainUser,Created_On,last_modified) values('{0}','{1}','{2}','{3}',GetDate(),GetDate())";
        //            query = string.Format(query, loginID.Trim(), password, displayName.Trim(), displayName.Trim());
        //            result = DbHelper.ExecuteNonQuery(query);
        //        }
        //        if (result > 0)
        //        {
        //            query = "select IDENT_CURRENT('busdta.synUserMaster')";
        //            userID = DbHelper.ExecuteScalar(query).ToString();
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager AddApplciationUser" + ex.Message);
        //        throw ex;
        //    }
        //    return userID;
        //}

        //public string EditApplciationUser(string displayName, string loginID, string password, int flag, int id)
        //{
        //    string query = string.Empty;
        //    string userID = string.Empty;
        //    int result = -1;

        //    try
        //    {
        //        logger.Info("UserManager AddApplciationUser parameters: " + displayName + " " + loginID + " " + password);
        //        //query = "select count(app_user) from busdta.synUserMaster where app_user='{0}'";
        //        //query = string.Format(query, loginID.Trim());
        //        //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
        //        //if (result == 0)
        //        //{
        //        //   query = "insert into busdta.synUserMaster(app_user,appPassword,Name,DomainUser) values('{0}','{1}','{2}','{3}')";
        //        query = "Update busdta.synUserMaster set app_user='" + loginID.Trim() + "',appPassword='" + password + "',Name='" + displayName + "',DomainUser='" + loginID + "',active=" + flag + " where App_user_id='" + id + "'";
        //        //query = string.Format(query, loginID.Trim(), password, displayName.Trim(), displayName.Trim());
        //        result = DbHelper.ExecuteNonQuery(query);
        //        //}
        //        //if (result > 0)
        //        //{
        //        //    query = "select IDENT_CURRENT('busdta.synUserMaster')";
        //        //    userID = DbHelper.ExecuteScalar(query).ToString();
        //        //}

        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager AddApplciationUser" + ex.Message);
        //        throw ex;
        //    }
        //    return userID;
        //}
        //public bool AddApplciationUserRole(string userID, string role)
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager AddApplciationUserRole parameters: " + userID + " " + role);
        //        query = "insert into busdta.User_Role_Map(app_user_id,Role,last_modified) values('{0}','{1}',{2})";
        //        query = string.Format(query, userID, role, DateTime.Now.ToShortDateString());
        //        int result = DbHelper.ExecuteNonQuery(query);
        //        return result > 0 ? true : false;
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager AddApplciationUserRole" + ex.Message);
        //        throw ex;
        //    }
        //}

        //public bool EditApplciationUserRole(int userID, string role)
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager AddApplciationUserRole parameters: " + userID + " " + role);
        //        query = "update busdta.User_Role_Map set Role='" + role + "',last_modified=GetDate() where App_user_id=" + userID + "";
        //        //  query = string.Format(query, userID, role, DateTime.Now.ToShortDateString());
        //        int result = DbHelper.ExecuteNonQuery(query);
        //        return result > 0 ? true : false;
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager EditApplciationUserRole" + ex.Message);
        //        throw ex;
        //    }
        //}
        //public bool UpdateApplciationUser(string state, string loginID)
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager UpdateApplciationUser parameters: " + state + " " + loginID);
        //        int toggleTo = state.ToLower() == "on" ? 1 : 0;
        //        query = "update busdta.synUserMaster set active=" + toggleTo + " where app_user_id='" + loginID + "'";
        //        int result = DbHelper.ExecuteNonQuery(query);
        //        return result > 0 ? true : false;
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager UpdateApplciationUser" + ex.Message);
        //        throw ex;
        //    }
        //}
        //public bool MapApplciationUserToRoute(string userID, string routeID)
        //{
        //    string query = string.Empty;
        //    int result = -1;
        //    int result1;
        //    try
        //    {
        //        logger.Info("UserManager MapApplciationUserToRoute parameters: " + userID + " " + routeID);
        //        query = "select count(App_user_id) from BUSDTA.Route_User_Map where App_user_id ='{0}' and Route_Id ='{1}'";
        //        query = string.Format(query, userID, routeID.Trim());
        //        result1 = Convert.ToInt32(DbHelper.ExecuteScalar(query));
        //        if (result1 == 0)
        //        {
        //            query = "insert into BUSDTA.Route_User_Map (App_user_id,Route_Id,Active,last_modified) values('{0}','{1}',0,'{2}')";
        //            query = string.Format(query, userID, routeID.Trim(), DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
        //            result = DbHelper.ExecuteNonQuery(query);
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager MapApplciationUserToRoute" + ex.Message);
        //        throw ex;
        //    }
        //    return result > 0 ? true : false;
        //}
        //public IEnumerable<SelectListItem> GetUserRoles()
        //{
        //    try
        //    {
        //        logger.Info("UserManager GetUserRoles");
        //        List<SelectListItem> list = new List<SelectListItem>();
        //        list.Add(new SelectListItem { Text = "RSR", Value = "RSR" });
        //        list.Add(new SelectListItem { Text = "RSM", Value = "RSM" });
        //        list.Add(new SelectListItem { Text = "DSM", Value = "DSM" });
        //        list.Add(new SelectListItem { Text = "Administrator", Value = "ITADMIN" });
        //        return new SelectList(list, "Value", "Text");
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager GetUserRoles");
        //        throw ex;
        //    }
        //}
        //public List<LoginViewModel> GetApplciationUsers()
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager GetApplciationUsers");
        //        query = "select u.Active, u.app_user as UserName,u.Name as DisplayName, u.appPassword as Password, u.app_user_id + '' as ID,rm.Role,u.last_modified as CreatedOn, rum.Route_ID as Route from busdta.synUserMaster u ";
        //        query = query + "left outer join BUSDTA.User_Role_Map rm  ";
        //        query = query + "left outer join BUSDTA.Route_User_Map rum  ";
        //        query = query + "on rm.app_user_id = rum.app_user_id  ";
        //        query = query + "on rm.App_user_id = u.App_user_id  order by u.last_modified desc";
        //        DataSet result = DbHelper.ExecuteDataSet(query);
        //        return result.GetEntityList<LoginViewModel>();
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager GetApplciationUsers");
        //        throw ex;

        //    }
        //}

        //public List<LoginViewModel> GetApplciationUsersbyID(int id)
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager GetApplciationUsers");
        //        query = "select u.Active, u.app_user as UserName,u.Name as DisplayName, u.appPassword as Password, u.app_user_id + '' as ID,rm.Role,u.last_modified as CreatedOn, rum.Route_ID as Route from busdta.synUserMaster u ";
        //        query = query + "left outer join BUSDTA.User_Role_Map rm  ";
        //        query = query + "left outer join BUSDTA.Route_User_Map rum  ";
        //        query = query + "on rm.app_user_id = rum.app_user_id  ";
        //        query = query + "on rm.App_user_id = u.App_user_id where app_user_id=" + id + "  order by u.last_modified desc";
        //        DataSet result = DbHelper.ExecuteDataSet(query);
        //        return result.GetEntityList<LoginViewModel>();
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager GetApplciationUsers");
        //        throw ex;

        //    }
        //}
        //public IEnumerable<SelectListItem> GetApplciationUserSelectList()
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    string query = string.Empty;
        //    try
        //    {
        //        logger.Info("UserManager GetApplciationUserSelectList");
        //        query = "select u.Active, u.app_user as UserName,u.Name as DisplayName, u.appPassword as Password, u.app_user_id + '' as ID,rm.Role,u.last_modified as CreatedOn from busdta.synUserMaster u ";
        //        query = query + "left outer join BUSDTA.User_Role_Map rm  ";
        //        query = query + "on rm.App_user_id = u.App_user_id  order by u.last_modified desc";
        //        DataSet result = DbHelper.ExecuteDataSet(query);
        //        if (result.HasData())
        //        {
        //            foreach (DataRow route in result.Tables[0].Rows)
        //            {
        //                list.Add(new SelectListItem { Text = route["UserName"].ToString(), Value = route["ID"].ToString() });
        //            }
        //        }
        //        return list;
        //    }
        //    catch (Exception ex)
        //    {

        //        logger.Error("Error in UserManager GetApplciationUserSelectList" + ex.Message);
        //        throw ex;

        //    }
        //}
        
    }
}
