﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using System.Data;
using System.Text;
using System.Web.Mvc;
using SLEAdminPortal.Helpers;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Managers
{
    public class AdminManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<AdminRole> GetRoleList()
        {
            try
            {
                logger.Info("AdminManager GetRoleListList");
                StringBuilder MyRoleQuery = new StringBuilder();
                MyRoleQuery.Append("SELECT rm.RoleID,rm.RoleName,rm.RoleDescription,rm.Active FROM BUSDTA.RoleMaster rm WITH(NOLOCK)");
                DataSet dsRoles = DbHelper.ExecuteDataSet(MyRoleQuery.ToString(), "SystemDB");
                List<AdminRole> lstRoles = new List<AdminRole>();
                if (dsRoles.HasData())
                {
                    lstRoles = dsRoles.GetEntityList<AdminRole>();
                }
                return lstRoles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AdminRole> GetSpecificRoleList(string RoleID, string RoleName, string Description)
        {
            try
            {
                logger.Info("AdminManager GetSpecificRoleList");
                StringBuilder MyRoleQuery = new StringBuilder();
                MyRoleQuery.Append("SELECT rm.RoleID,rm.RoleName,rm.RoleDescription,rm.Active FROM BUSDTA.RoleMaster rm WITH(NOLOCK) WHERE 1=1");
                if (string.IsNullOrEmpty(RoleID) != true && RoleID.Trim() != "")
                    MyRoleQuery.Append("AND rm.RoleID LIKE '%" + RoleID + "%'");
                if (string.IsNullOrEmpty(RoleName) != true && RoleName.Trim() != "")
                    MyRoleQuery.Append("AND rm.RoleName LIKE '%" + RoleName + "%'");
                if (string.IsNullOrEmpty(Description) != true && Description.Trim() != "")
                    MyRoleQuery.Append("AND rm.RoleDescription LIKE '%" + Description + "%'");
                DataSet dsRoles = DbHelper.ExecuteDataSet(MyRoleQuery.ToString(), "SystemDB");
                List<AdminRole> lstRoles = new List<AdminRole>();
                if (dsRoles.HasData())
                {
                    lstRoles = dsRoles.GetEntityList<AdminRole>();
                }
                return lstRoles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertNewRole(AdminRoleModel model)
        {
            string MyQuery = string.Empty;
            int result = -1; int Weightage;
            try
            {
                MyQuery = "SELECT COUNT(*) FROM BUSDTA.RoleMaster rm WHERE rm.RoleName='{0}'";
                MyQuery = string.Format(MyQuery, model.RoleName);
                result = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
                if (result == 0)
                {
                    Weightage = GetRoleWeightage();
                    MyQuery = "INSERT INTO BUSDTA.RoleMaster(RoleName,RoleDescription,Active,CreatedBy,CreatedDate) VALUES('{0}','{1}',,1, 1, GETDATE())";
                    MyQuery = string.Format(MyQuery, model.RoleName, model.RoleDescription);
                    result = DbHelper.ExecuteNonQuery(MyQuery);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result > 0 ? true : false;
        }

        private int GetRoleWeightage()
        {
            return 0;

            //string MyQuery = string.Empty;
            //int result = -1;
            //try
            //{
            //    MyQuery = "SELECT MAX(Weightage)+1 FROM BUSDTA.RoleMaster";
            //    result = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
            //    return result;
            //}
            //catch(Exception)
            //{
            //    throw;
            //}
        }

        public bool UpdateRole(string state, string roleID)
        {
            int result = -1;
            try
            {
                logger.Info("AdminManager UpdateDevice Parameters: " + state + " " + roleID);
                string MyQuery = string.Empty;
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                MyQuery = "UPDATE BUSDTA.RoleMaster SET Active=" + toggleTo + " WHERE RoleID='" + roleID + "'";
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in AdminManager UpdateDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public List<AdminRole> DeleteRole(string RoleID)
        {
            try
            {
                logger.Info(" AdminManager DeleteRole");
                string query = string.Empty;
                query = query + "Delete from Busdta.RoleMaster where RoleID=" + RoleID;
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<AdminRole> objAdmin = new List<AdminRole>();
                if (dsEnv.HasData())
                {
                    objAdmin = dsEnv.GetEntityList<AdminRole>();
                }
                return objAdmin;

            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager DeleteUser" + ex.Message);
                throw ex;
            }
        }

    }
}