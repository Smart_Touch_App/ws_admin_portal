﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Web;
using SLEAdminPortal.Models;
using System.Web.Mvc;
using System.Data;
using log4net;
using System.Data.SqlClient;
using System.Configuration;

namespace SLEAdminPortal.Managers
{
    public class EnvironmentManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Environmentmaster> GetEnvironmnetList()
        {
            try
            {
                //Listing Environment Details From DB:
                logger.Info(" EnvironmentManager GetEnvironmnetList");
                string query = string.Empty;
                query = query + "select * from BUSDTA.Environment_Master";
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<Environmentmaster> Environment = new List<Environmentmaster>();
                if (dsEnv.HasData())
                {
                    Environment = dsEnv.GetEntityList<Environmentmaster>();
                }
                return Environment;
            }
            catch (Exception ex)
            {
                logger.Error("Error in EnvironmentManager GetEnvironmnetList" + ex.Message);
                throw ex;
            }


        }
        public Environmentmaster GetById(int EnvironmentMasterId)
        {
            try
            {
                //Selecting individual ID from DB display the Details of Environment:
                logger.Info(" EnvironmentManager GetById");
                string query = string.Empty;
                query = query + " select * from BUSDTA.Environment_Master where EnvironmentMasterId=" + EnvironmentMasterId;
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                Environmentmaster Environment = new Environmentmaster();
                if (dsEnv.HasData())
                {
                    Environment = dsEnv.GetEntity<Environmentmaster>();
                }
                return Environment;
            }
            catch (Exception ex)
            {
                logger.Error("Error in EnvironmentManager GetById" + ex.Message);
                throw ex;
            }

        }
        /*  public bool SaveEnvironment(EnvironmentModels env)
          {
            
              try
              {
                  logger.Info(" EnvironmentManager SaveEnvironment parameters:");
                  string query = string.Empty;
                  query =  "Insert into Busdta.Environment_Master(EnvName,EnvDescription,IsActive,ReleaseLevel,MLServerAddress,JDEWSAddress,REFDBAddress,ENVDBAddress,CDBAddress,CDBConKey,IISAddress,AppInstallerLocation,RDBScriptLocation,TaxLibLocation,CreatedBy,CreatedDatetime,PortalConnectionString) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')";
                  query = string.Format(query, env.EnvName, env.EnvDescription, "1", env.ReleaseLevel, env.MLServerAddress, env.JDEWSAddress, env.REFDBAddress, env.ENVDBAddress, env.CDBAddress, env.CDBConKey, env.IISAddress, env.AppInstallerLocation, env.RDBScriptLocation, env.TaxLibLocation, "1", DateTime.Now.ToShortDateString(), env.PortalConnectionString);
                  int result = DbHelper.ExecuteNonQuery(query);
                  return result > 0 ? true : false;
              }
          
              catch (Exception ex)
              {
                  logger.Error("Error in EnvironmentManager SaveEnvironmnet" + ex.Message);
                  throw ex;
              }
          }*/
        /*   public bool EditEnvironment(EnvironmentModels env)
           {

               try
               {
                   //View Environment Details from DB:
                   logger.Info(" EnvironmentManager EditEnvironment parameters:");
                   string query = string.Empty;
                   query = "update BUSDTA.Environment_Master set EnvName ='" + env.EnvName + "', EnvDescription='" + env.EnvDescription + "', IsActive='" + env.IsActive + "',ReleaseLevel='" + env.ReleaseLevel + "',MLServerAddress='" + env.MLServerAddress + "',JDEWSAddress='" + env.JDEWSAddress + "', REFDBAddress='" + env.REFDBAddress + "',ENVDBAddress='" + env.ENVDBAddress + "',CDBAddress='" + env.CDBAddress + "',CDBConKey='" + env.CDBConKey + "',IISAddress='" + env.IISAddress + "',AppInstallerLocation='" + env.AppInstallerLocation + "',RDBScriptLocation='" + env.RDBScriptLocation + "',TaxLibLocation='" + env.TaxLibLocation + "',UpdatedBy=1,UpdatedDatetime=GETDATE(),PortalConnectionString='" + env.PortalConnectionString + "' where EnvironmentMasterId=" + env.EnvironmentMasterId;
                   int result = DbHelper.ExecuteNonQuery(query);
                   return result > 0 ? true : false;
               }

               catch (Exception ex)
               {
                   logger.Error("Error in EnvironmentManager ViewEnvironmnet" + ex.Message);
                   throw ex;
               }
           }*/

        /* public bool UpdateRole(string state, string EnvironmentMasterId)
         {
             int result = -1;
             try
             {
                 logger.Info("Environmentmaster UpdateEnvironment Parameters: " + state + " " + EnvironmentMasterId);
                 string MyQuery = string.Empty;
                 int toggleTo = state.ToLower() == "on" ? 1 : 0;
                 MyQuery = "UPDATE BUSDTA.Environment_Master SET IsActive=" + toggleTo + " WHERE EnvironmentMasterId='" + EnvironmentMasterId + "'";
                 result = DbHelper.ExecuteNonQuery(MyQuery);
             }
             catch (Exception ex)
             {
                 logger.Error("Error in EnvironmentManager UpdateEnvironmente" + ex.Message);
                 throw ex;
             }
             return result > 0 ? true : false;
         }*/
        /*  public List<Environmentmaster> DeleteEnvironment(int EnvironmentMasterId)
          {
              try
              {
                  logger.Info(" EnvironmentManager DeleteEnvironment");
                  string query = string.Empty;
                  query = query + "Delete from Busdta.Environment_Master where EnvironmentMasterId=" + EnvironmentMasterId;
                  DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                  List<Environmentmaster> Environment = new List<Environmentmaster>();
                  if (dsEnv.HasData())
                  {
                      Environment = dsEnv.GetEntityList<Environmentmaster>();
                  }
                  return Environment;

              }
              catch (Exception ex)
              {
                  logger.Error("Error in EnvironmentManager DeleteEnvironmnet" + ex.Message);
                  throw ex;
              }
          }*/

    }
}
