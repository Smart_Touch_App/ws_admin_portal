﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Data;
using SLEAdminPortal.Models;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace SLEAdminPortal.Managers
{
    public class DeviceManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string AddDeviceInformation(string FQDN, string MachineName, string Environment, string Status, string Manufacturer, string Model)
        {
            string query = string.Empty, query1 = string.Empty, query2 = string.Empty;
            string MachineID = "";
            int result = -1, result2 = -1;
            try
            {

                logger.Info("UserManager AddDeviceInformation parameters: " + MachineName + " " + FQDN + " " + Environment);
                query = "select COUNT(*) from BUSDTA.MachineMaster where FQDN = '" + FQDN + "' or ComptuerName='" + MachineName + "'";

                result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));

                if (result == 0)
                {
                    query = "INSERT INTO BUSDTA.MachineMaster(FQDN,ComptuerName,Active,manufacturer,model) VALUES ('" + FQDN + "','" + MachineName + "',1,'" + Manufacturer + "','" + Model + "') ";

                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");

                }
                else
                {
                    throw new Exception("ComputerName or FQDN Already Exists");

                }

                if (result > 0)
                {
                    query = "select COUNT(FQDN) from BUSDTA.MachineMaster";
                    query += " where FQDN = '{0}'";
                    query = string.Format(query, FQDN.Trim());
                    MachineID = DbHelper.ExecuteScalar(query, null, "SystemDB").ToString();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserManager AddDeviceInformation" + ex.Message);
                throw ex;
            }

            return MachineID;
        }

        public string UpdateDeviceInfo(string OldMachineID, string FQDN, string MachineName, string manufac, string model)
        {
            string query = string.Empty, query1 = string.Empty, UpdateDeviceInfo = string.Empty;
            int result = -1; int result1 = -1;
            //       query = "select COUNT(Device_ID) from BUSDTA.MachineMaster where Device_ID = '" + OldMachineID + "'";
            //    query = "select COUNT(ComptuerName) from BUSDTA.MachineMaster WHERE ComptuerName='" + MachineName + "'";
            query = "select COUNT(*) from BUSDTA.MachineMaster WHERE ComptuerName='" + MachineName + "' or FQDN='" + FQDN + "'";
            result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));
            if (result == 0)
            {
                query = " Update BUSDTA.MachineMaster SET FQDN='" + FQDN + "', ComptuerName='" + MachineName + "', manufacturer='" + manufac + "', model='" + model + "' where Device_ID='" + OldMachineID + "'";
                UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
            }
            else if (result > 0)
            {
                query = "select COUNT(*) from BUSDTA.MachineMaster WHERE ComptuerName='" + MachineName + "'";
                result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));
                query = "select COUNT(*) from BUSDTA.MachineMaster WHERE FQDN='" + FQDN + "'";
                result1 = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));
                if (result == 0 && result1 == 0)
                {
                    query = " Update BUSDTA.MachineMaster SET ComptuerName='" + MachineName + "', FQDN='" + FQDN + "', manufacturer='" + manufac + "', model='" + model + "' where Device_ID='" + OldMachineID + "'";
                    UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
                }

                else if (result == 0 && result1 == 1)
                {
                    query = " Update BUSDTA.MachineMaster SET  ComptuerName='" + MachineName + "', manufacturer='" + manufac + "', model='" + model + "' where Device_ID='" + OldMachineID + "'";
                    UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
                }
                else if (result == 1 && result1 == 0)
                {
                    query = " Update BUSDTA.MachineMaster SET  FQDN='" + FQDN + "', manufacturer='" + manufac + "', model='" + model + "' where Device_ID='" + OldMachineID + "'";
                    UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
                }
                else
                {
                    throw new Exception("Computer Name or FQDN Already Exists");
                }


            }
            else
            {
                throw new Exception("Computer Name or FQDN Already Exists");
            }
            //query = "select COUNT(FQDN) from BUSDTA.MachineMaster where FQDN = '" + FQDN + "'";
            //result = Convert.ToInt32(DbHelper.ExecuteScalar(query, null, "SystemDB"));

            //if (result == 0)
            //{
            //    query = " Update BUSDTA.MachineMaster SET FQDN='" + FQDN + "', ComptuerName='" + MachineName + "', manufacturer='" + manufac + "', model='" + model + "' where Device_ID='" + OldMachineID + "'";
            //    UpdateDeviceInfo = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB").ToString();
            //}
            //else
            //{
            //    throw new Exception("FQDN Already Exists");
            //}
            return UpdateDeviceInfo;
        }

        //private string getStatus(string deviceid, string EnvId)
        //{
        //    try
        //    {
        //        string str = null;
        //        string query1 = string.Empty;

        //        string[] arrOldEnv = EnvId.Split(',');
        //        foreach (var i in arrOldEnv)
        //        {
        //            if (!string.IsNullOrEmpty(i))
        //            {
        //                query1 = "select RouteID, ActiveYN from BUSDTA.RemoteDB_Master where DeviceID='" + deviceid + "' and EnvironmentMasterID=" + Convert.ToInt32(i);
        //                DataSet ds1 = DbHelper.ExecuteDataSet(query1, "SystemDB");
        //                RemoteDbNew lst1 = new RemoteDbNew();
        //                if (ds1.HasData())
        //                {
        //                    lst1 = ds1.GetEntity<RemoteDbNew>();
        //                }
        //                if (lst1 == null)
        //                {
        //                    str += str == null ? "Ready To Onboard" : ",Ready To Onboard";
        //                }
        //                else
        //                {
        //                    str += str == null ? lst1.ActiveYN == "Y" ? "Onboarded" : lst1.ActiveYN == "Y" ? "Reserved" : "Ready To Onboard" : lst1.ActiveYN == "Y" ? ",Onboarded" : lst1.ActiveYN == "Y" ? ",Reserved" : ",Ready To Onboard";
        //                }
        //            }
        //        }

        //        return str;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}


        private DeviceStatusRoute getRouteStatus(string deviceid, string EnvId)
        {
            List<DeviceStatusRoute> newLst = new List<DeviceStatusRoute>();
            DeviceStatusRoute newItem = new DeviceStatusRoute();
            try
            {
                string query1 = string.Empty;

                string[] arrOldEnv = EnvId.Split(',');
                foreach (var i in arrOldEnv)
                {
                    DeviceStatusRoute lst1 = new DeviceStatusRoute();

                    if (!string.IsNullOrEmpty(i))
                    {
                        //query1 = "select RouteID, ActiveYN from BUSDTA.RemoteDB_Master where DeviceID='" + deviceid + "' and EnvironmentMasterID=" + Convert.ToInt32(i);

                        query1 = @"Declare @RouteID varchar(100)
Declare @DBName varchar(100)
Declare @UserID varchar(100)
select @DBName = CDBAddress FROM BUSDTA.Environment_Master WHERE EnvironmentMasterId=" + Convert.ToInt32(i) + @" 
select @RouteID = RouteID FROM Busdta.RemoteDB_Master where DeviceID='" + deviceid + "' and EnvironmentMasterID=" + Convert.ToInt32(i) + @" and ActiveYN='Y'  
select @UserID = AppUserId FROM Busdta.RemoteDB_Master where DeviceID='" + deviceid + "' and EnvironmentMasterID=" + Convert.ToInt32(i) + @" and ActiveYN='Y' 
if @DBName is not null and @RouteID is not null and @UserID is not null
EXEC(
	'USE ' + @DBName
	+ 'SELECT distinct case when RS.RouteStatus= ''REGISTERED'' then ''OnBoarded'' when RS.RouteStatus=''UNREGISTERED'' then ''Ready To Onboard'' else RS.RouteStatus end as RouteStatus, RM.RouteName,
    (select top 1 Isnull(c.VersionNumber,''NIL'')  from SystemDb.busdta.RemoteDB_Master a, SystemDB.dbo.tblRemoteDBRelease b,
     SystemDB.dbo.tblAppReleaseMaster c where a.RDBReleaseID=b.RDBReleaseID and b.SLEAppReleaseID=c.AppReleaseID 
	 and a.RouteID=''' + @RouteID +  ''' and a.EnvironmentMasterID=" + Convert.ToInt32(i) + @" and a.ActiveYN=''Y'' ) as AppVersionNumber,
	 (select top 1 ISNULL(CONVERT(VARCHAR(25),c.ReleasedDate,101),''NIL'') from SystemDb.busdta.RemoteDB_Master a, SystemDB.dbo.tblRemoteDBRelease b,
     SystemDB.dbo.tblAppReleaseMaster c where a.RDBReleaseID=b.RDBReleaseID and b.SLEAppReleaseID=c.AppReleaseID 
	 and a.RouteID=''' + @RouteID +  ''' and a.EnvironmentMasterID=" + Convert.ToInt32(i) + @" and a.ActiveYN=''Y'' ) as AppReleasedDate
FROM BUSDTA.RouteSecurity AS RUM 
INNER JOIN BUSDTA.Route_Master AS RM ON RUM.RouteID = RM.RouteMasterID 
LEFT JOIN BUSDTA.RouteStatus AS RS ON RM.RouteStatusID=RS.RouteStatusID 
WHERE RouteName=''' + @RouteID +  '''' 
)";
                        try
                        {
                            DataSet ds1 = DbHelper.ExecuteDataSet(query1, "SystemDB");
                            if (ds1.HasData())
                            {
                                lst1 = ds1.GetEntity<DeviceStatusRoute>();
                                if (lst1.RouteStatus != "OnBoarded")
                                {
                                    lst1.RouteName = "NIL";
                                    lst1.AppReleasedDate = "NIL";
                                    lst1.AppVersionNumber = "NIL";
                                }
                            }
                            else
                            {
                                lst1.RouteName = "NIL";
                                lst1.RouteStatus = "Ready To Onboard";
                                lst1.AppReleasedDate = "NIL";
                                lst1.AppVersionNumber = "NIL";
                            }
                        }
                        catch (Exception)
                        {
                            lst1.RouteName = "NIL";
                            lst1.RouteStatus = "Ready To Onboard";
                            lst1.AppReleasedDate = "NIL";
                            lst1.AppVersionNumber = "NIL";
                        }
                    }

                    newLst.Add(lst1);
                }

                //if (string.IsNullOrEmpty(lst1.RouteStatus))
                //{
                //    lst1.RouteStatus = "Ready To Onboard";
                //    lst1.RouteName = "NIL";
                //}

                var sb = new StringBuilder();
                var sb1 = new StringBuilder();
                var sb2 = new StringBuilder();
                var sb3 = new StringBuilder();
                string separator = String.Empty;
                foreach (var value in newLst)
                {
                    sb.Append(separator).Append(value.RouteName);
                    sb1.Append(separator).Append(value.RouteStatus);
                    sb2.Append(separator).Append(value.AppReleasedDate);
                    sb3.Append(separator).Append(value.AppVersionNumber);
                    separator = ",";
                }
                newItem.RouteName = sb.ToString();
                newItem.RouteStatus = sb1.ToString();
                newItem.AppReleasedDate = sb2.ToString();
                newItem.AppVersionNumber = sb3.ToString();
            }
            catch (Exception)
            {
                //if (string.IsNullOrEmpty(lst1.RouteStatus))
                //{
                //    lst1.RouteStatus = "Ready To Onboard";
                //    lst1.RouteName = "NIL";
                //}
                //return lst1;

            }
            return newItem;
        }
        public List<Device> GetDeviceList()
        {
            try
            {
                logger.Info("RouteManager GetDeviceList");
                string query2 = string.Empty;

                query2 = query2 + " SELECT LTRIM(RTRIM(Device_Id)) AS 'MachineID',FQDN,ComptuerName, ";
                query2 = query2 + "   STUFF((SELECT ', ' + Convert(varchar(100),EM.EnvironmentMasterId)  FROM BUSDTA.Device_Env_Map DEM  " +
                                    " LEFT JOIN BUSDTA.Environment_Master EM " +
                                    " ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId  " +
                                    " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'EnvironmentId',";

                query2 = query2 + " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.Device_Env_Map DEM ";
                query2 = query2 + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
                query2 = query2 + " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'Environment',  ";
                query2 = query2 + " RDM.RouteID AS Route ,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status, Active  ";
                query2 = query2 + " FROM BUSDTA.MachineMaster DM ";
                query2 = query2 + " LEFT JOIN BUSDTA.RemoteDB_Master RDM ON RDM.DeviceID = DM.Device_Id and RDM.ActiveYN='Y'";
                query2 = query2 + " GROUP BY Device_Id,FQDN,ComptuerName,RouteID,ActiveYN,Active,dm.last_modified";
                query2 = query2 + " ORDER BY dm.last_modified DESC ";

                DataSet dsDevice2 = DbHelper.ExecuteDataSet(query2, "SystemDB");
                List<Device> devices1 = new List<Device>();
                if (dsDevice2.HasData())
                {
                    devices1 = dsDevice2.GetEntityList<Device>();
                }

                List<Device> newDev = new List<Device>();
                foreach (var i in devices1)
                {
                    var rs = getRouteStatus(i.MachineID, i.EnvironmentId);

                    newDev.Add(new Device
                    {
                        Active = i.Active,
                        DeviceID = i.DeviceID,
                        Environment = i.Environment,
                        EnvironmentId = i.EnvironmentId,
                        MachineID = i.MachineID,
                        ComptuerName = i.ComptuerName,
                        FQDN = i.FQDN,
                        Manufacturer = i.Manufacturer,
                        Route = rs.RouteName,
                        Status = rs.RouteStatus,
                        ReleasedDate = rs.AppReleasedDate,
                        VersionNumber = rs.AppVersionNumber
                    });
                }


                return newDev;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetDeviceList" + ex.Message);
                throw ex;
            }
        }
        public List<Device> DeviceDeregister(string MachineID, string Route)
        {
            try
            {
                string ConnectionString = string.Empty;
                ConnectionString = ConfigurationManager.ConnectionStrings["SystemDB"].ToString();
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                SqlCommand cmd = new SqlCommand(ConnectionString);
                Int32 rowsAffected;
                cmd.CommandText = "uspDeregisterRouteDeviceV2";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = MachineID;
                cmd.Parameters.Add("@RouteName", SqlDbType.VarChar).Value = Route;
                cmd.Connection = dbConnection;
                dbConnection.Open();
                rowsAffected = cmd.ExecuteNonQuery();
                dbConnection.Close();
                List<Device> devices = new List<Device>();
                return devices;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager DeviceDeregister" + ex.Message);
                throw ex;
            }

        }
        public List<Device> DeviceInfo(string MachineID)
        {
            string query = string.Empty;
            query = query + " SELECT DM.ComptuerName,DM.FQDN,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status,  DM.manufacturer as Manufacturer, DM.model as Model ";
            query = query + " FROM BUSDTA.MachineMaster DM ";
            //query = query + " INNER JOIN BUSDTA.Device_Environment_Map DEM ON DEM.DeviceId = DM.Device_Id ";
            //query = query + " INNER JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
            query = query + " LEFT JOIN BUSDTA.RemoteDB_Master RDM ON RDM.DeviceID = DM.Device_Id ";
            query = query + " WHERE Device_Id = '" + MachineID + "'";
            DataSet dsDeviceInfo = DbHelper.ExecuteDataSet(query, "SystemDB");
            List<Device> DeviceInfo = new List<Device>();
            if (dsDeviceInfo.HasData())
            {
                DeviceInfo = dsDeviceInfo.GetEntityList<Device>();
            }
            return DeviceInfo;
        }
        public string ChangeActiveFlag(string MachineID, string Active)
        {
            try
            {
                int result = -1;
                string query = string.Empty;
                string userID = string.Empty;
                if (Active == "0")
                {
                    query = query + " UPDATE BUSDTA.MachineMaster SET Active = 1 WHERE Device_Id ='" + MachineID + "' ";
                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");
                }
                else if (Active == "1")
                {
                    query = query + " UPDATE BUSDTA.MachineMaster SET Active = 0 WHERE Device_Id ='" + MachineID + "' ";
                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");
                }
                return userID;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager DeviceDeregister" + ex.Message);
                throw ex;
            }

        }

        public List<Device> GetSpecificDeviceList(string FQDN, string MachineName, string Environment)
        {
            try
            {
                //logger.Info("RouteManager GetSpecificDeviceList");
                //string query = string.Empty;
                //query = query + " SELECT LTRIM(RTRIM(Device_Id)) AS 'MachineID',DeviceName AS 'MachineName', ";
                //query = query + " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.Device_Environment_Map DEM ";
                //query = query + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
                //query = query + " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'Environment',  ";
                //query = query + " RDM.RouteID AS Route ,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status, Active  ";
                //query = query + " FROM BUSDTA.Device_Master DM ";
                //query = query + " LEFT JOIN BUSDTA.RemoteDBID_Master RDM ON RDM.DeviceID = DM.Device_Id ";
                //query = query + " LEFT JOIN BUSDTA.Device_Environment_Map DEM on DEM.EnvironmentMasterId = RDM.EnvironmentMasterID ";
                //query = query + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId  ";
                //query = query + " where 1=1 ";
                //if (!string.IsNullOrEmpty(DeviceID))
                //{
                //    query = query + " and Device_Id='" + DeviceID + "'";
                //}
                //if (!string.IsNullOrEmpty(MachineName))
                //{
                //    query = query + " and DeviceName='" + MachineName + "'";
                //}
                //if (!string.IsNullOrEmpty(Environment))
                //{
                //    query = query + " and EM.CDBConKey='" + Environment + "'";
                //}
                //query = query + " GROUP BY Device_Id, DeviceName,RouteID,ActiveYN,Active,dm.last_modified";
                //query = query + " ORDER BY dm.last_modified DESC ";
                //DataSet dsDevice = DbHelper.ExecuteDataSet(query, "SystemDB");
                //List<Device> devices = new List<Device>();
                //if (dsDevice.HasData())
                //{
                //    devices = dsDevice.GetEntityList<Device>();
                //}
                //return devices;

                var lst = GetDeviceList();

                if (!string.IsNullOrEmpty(FQDN))
                    lst = lst.Where(x => x.FQDN.ToLower().Contains(FQDN.ToLower())).OrderByDescending(x => x.MachineID).ToList();

                if (!string.IsNullOrEmpty(MachineName))
                    lst = lst.Where(x => x.ComptuerName.ToLower().Contains(MachineName.ToLower())).OrderByDescending(x => x.MachineID).ToList();

                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetSpecificDeviceList" + ex.Message);
                throw ex;
            }
        }


        public IEnumerable<SelectListItem> GetEnvironment()
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                string query = string.Empty;
                query = query + "select EnvName from BUSDTA.Environment_Master";
                DataSet dsEnvironment = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<SLEAdminPortal.Models.Environment> environment = new List<SLEAdminPortal.Models.Environment>();
                if (dsEnvironment.HasData())
                {
                    foreach (DataRow envmnt in dsEnvironment.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = envmnt["EnvName"].ToString(), Value = envmnt["EnvName"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetEnvironment" + ex.Message);
                throw ex;
            }
        }


        public bool SaveDeviceEnvironmentMapping(int EnvironmentMasterId, string DeviceId)
        {
            try
            {
                logger.Info("Device Environment Mapping Save");
                string query = string.Empty;

                query = " if not exists (select DeviceId from [BUSDTA].[Device_Env_Map] where DeviceId='" + DeviceId + "' and EnvironmentMasterId=" + EnvironmentMasterId + ") Insert into BUSDTA.Device_Env_Map VALUES(" + EnvironmentMasterId + ",'" + DeviceId + "',1,GETDATE(),null,null)";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment Mapping Save" + ex.Message);
                throw ex;
            }
        }

        public bool DeleteDeviceEnvironmentMapping(string DeviceId, int EnvId)
        {
            try
            {
                logger.Info("DeviceEnvironmentMapping Delete");
                string query = string.Empty;

                query = "if not exists (select RemoteDBID from BUSDTA.RemoteDB_Master where DeviceID='" + DeviceId + "' and EnvironmentMasterID=" + EnvId + ")" +
                    " delete from BUSDTA.Device_Env_Map where EnvironmentMasterId=" + EnvId + " and DeviceId='" + DeviceId + "'";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceEnvironmentMapping Delete" + ex.Message);
                throw ex;
            }
        }

        public string GetEnvironmentByDeviceId(string DeviceId)
        {
            try
            {
                logger.Info("Device Environment Mapping Save");
                string query = string.Empty;

                query = " select STUFF((SELECT ', ' + CONVERT(varchar(100), EM.EnvironmentMasterId) FROM BUSDTA.Device_Env_Map DEM  LEFT JOIN BUSDTA.Environment_Master EM  ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId " +
                        " WHERE DEM.DeviceId = '" + DeviceId + "' FOR XML PATH('')), 1, 2, '') AS 'EnvironmentId'";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");

                return dsEnv.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment Mapping Save" + ex.Message);
                throw ex;
            }
        }

    }
}