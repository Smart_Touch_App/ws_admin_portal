﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEAdminPortal.Models;
using System.Data;
using SLEAdminPortal.Helpers;
using System.ServiceProcess;
using System.Text;
using sampleweb;
using System.Threading;

namespace SLEAdminPortal.Managers
{
    public class ServiceManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        
        public List<EmailConfig> GetConfigDetails()
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT * FROM BUSDTA.EmailConfig";
                DataSet dsjobs = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<EmailConfig> emaildetail = new List<EmailConfig>();
                if (dsjobs.HasData())
                {
                    emaildetail = dsjobs.GetEntityList<EmailConfig>();
                }

                return emaildetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveJobSequence(List<JobDetails> jb)
        {
            string MyQuery = string.Empty;
            int result = -1;
            try
            {
                bool IsActive = true;
                int IsJobExists;
                MyQuery = "Select count(JobName) from busdta.jobdetails where jobname='" + jb[0].JobName + "' and Isactive=1";
                IsJobExists = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
                if (IsJobExists > 0 && jb[0].JobID == 0)
                {
                    return false;
                }
                if (jb[0].JobID == 0)
                {
                    MyQuery = "Insert into BUSDTA.JobDetails(JobName,CreatedDate,LastModifiedDate,IsActive) VALUES ('" + jb[0].JobName + "','" + DateTime.Now + "','" + DateTime.Now + "','" + IsActive + "') ";
                    result = DbHelper.ExecuteNonQuery(MyQuery);
                    string Query1 = string.Empty;
                    Query1 = "Select max(JobID) from Busdta.Jobdetails";
                    int maxjobid = Convert.ToInt32(DbHelper.ExecuteScalar(Query1));
                    if (result > 0)
                    {
                        for (int i = 0; i < jb.Count; i++)
                        {
                            var jobid = maxjobid;
                            var sequenceno = jb[i].SequenceNo;
                            var modulename = jb[i].ModuleName;
                            var dependency = jb[i].Dependency;
                            var emailnotification = jb[i].EmailNotification;
                            var emailaddress = jb[i].EmailAddress;
                            string Query2 = string.Empty;
                            Query2 = "Insert into BUSDTA.ModuleSequence(JobID,SequenceNo,ModuleName,Dependency,EmailNotification,EmailConfigID)VALUES('" + jobid + "','" + sequenceno + "','" + modulename + "','" + dependency + "','" + emailnotification + "','" + emailaddress + "')";
                            result = DbHelper.ExecuteNonQuery(Query2);
                        }
                    }
                }
                else
                {
                    MyQuery = " UPDATE [BUSDTA].[JobDetails] SET [JobName] ='" + jb[0].JobName + "' ,[LastModifiedDate] ='" + DateTime.Now + "' WHERE JobID=" + jb[0].JobID;
                    result = DbHelper.ExecuteNonQuery(MyQuery);
                    string NonDeletemodule = "";
                    string Query2 = string.Empty;
                    if (result > 0)
                    {
                        for (int i = 0; i < jb.Count; i++)
                        {
                            string Query1 = string.Empty;
                            Query1 = "Select COUNT(*) from [BUSDTA].[ModuleSequence] where JobID=" + jb[0].JobID + " and ModuleName='" + jb[i].ModuleName + "'";
                            int maxjobid = Convert.ToInt32(DbHelper.ExecuteScalar(Query1));

                            var sequenceno = jb[i].SequenceNo;
                            var modulename = jb[i].ModuleName;
                            var dependency = jb[i].Dependency;
                            var emailnotification = jb[i].EmailNotification;
                            var emailaddress = jb[i].EmailAddress;

                            if (string.IsNullOrEmpty(NonDeletemodule))
                            {
                                NonDeletemodule = "'" + modulename + "'";
                            }
                            else
                            {
                                NonDeletemodule = NonDeletemodule + ",'" + modulename + "'";
                            }

                            if (maxjobid > 0)
                            {
                                Query2 = "UPDATE [BUSDTA].[ModuleSequence] SET [SequenceNo] =" + sequenceno + ",[Dependency] ='" + dependency + "',[EmailNotification] = '" + emailnotification + "',[EmailConfigID]= '" + emailaddress + "' WHERE [JobID] = " + jb[0].JobID + "AND [ModuleName] ='" + modulename + "'";
                            }
                            else
                            {
                                Query2 = "Insert into BUSDTA.ModuleSequence(JobID,SequenceNo,ModuleName,Dependency,EmailNotification,EmailConfigID)VALUES('" + jb[0].JobID + "','" + sequenceno + "','" + modulename + "','" + dependency + "','" + emailnotification + "','" + emailaddress + "')";
                            }
                            result = DbHelper.ExecuteNonQuery(Query2);
                        }
                        Query2 = "DELETE FROM [BUSDTA].[ModuleSequence]  WHERE [JobID] = " + jb[0].JobID + " AND NOT ModuleName IN(" + NonDeletemodule + ")";
                        result = DbHelper.ExecuteNonQuery(Query2);
                        result = 1;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result > 0 ? true : false;
        }


        public bool SaveEmailConfig(EmailConfig emailconfig)
        {
            string MyQuery = string.Empty;
            int result = -1;
            try
            {
                string strpassword = Encryptdata(emailconfig.Password);
                if (emailconfig.ID == 0)
                {
                    MyQuery = "INSERT INTO [BUSDTA].[EmailConfig]([GroupEmailName],[FromRecipients],[Password],[ToRecipients],[CcRecipients],[BccRecipients],[CreatedDate],[LastModifiedDate])VALUES ('" + emailconfig.GroupEmailName + "','" + emailconfig.FromRecipients + "','"+strpassword+"','" + emailconfig.ToRecipients + "','" + emailconfig.CcRecipients + "','" + emailconfig.BccRecipients + "','" + DateTime.Now + "','" + DateTime.Now + "')";
                }
                else
                {
                    MyQuery = "UPDATE [BUSDTA].[EmailConfig] SET [GroupEmailName] ='" + emailconfig.GroupEmailName + "',[FromRecipients] ='" + emailconfig.FromRecipients + "' ,[Password]='"+strpassword+"',[ToRecipients] ='" + emailconfig.ToRecipients + "' ,[CcRecipients] = '" + emailconfig.CcRecipients + "',[BccRecipients] ='" + emailconfig.BccRecipients + "' ,[LastModifiedDate] ='" + DateTime.Now + "' WHERE ID=" + emailconfig.ID;
                }
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result > 0 ? true : false;
        }


        private string Encryptdata(string password)
        {
            try
            {
                string strmsg = string.Empty;
                byte[] encode = new byte[password.Length];
                encode = Encoding.UTF8.GetBytes(password);
                strmsg = Convert.ToBase64String(encode);
                return strmsg;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        //Decryption
        public string Decryptdata(string encryptpwd)
        {
            try
            {
                string decryptpwd = string.Empty;
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                return decryptpwd;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public EmailConfig GetEditEmailConfig(string id)
        {
            string query1 = string.Empty;

            List<EmailConfig> EditEmail = new List<EmailConfig>();
            try
            {
                query1 = "SELECT  [ID],[GroupEmailName],[FromRecipients],[Password],[ToRecipients],[CcRecipients],[BccRecipients],[CreatedDate],[LastModifiedDate] FROM [SystemDB].[BUSDTA].[EmailConfig] WHERE ID =" + id;
                DataSet EmailConfig = DbHelper.ExecuteDataSet(query1, "SystemDB");
              
                if (EmailConfig.HasData())
                {
                  //  EmailConfig.Tables[0].Rows[0].
                    EditEmail = EmailConfig.GetEntityList<EmailConfig>();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return EditEmail.FirstOrDefault();
        }

        public Service GetEditSchedule(int ScheduleID)
        {
            string query1 = string.Empty;
            List<Service> EditSchedule = new List<Service>();
            try
            {
                query1 = @"
SELECT ScheduleID,JobID,EnvironmentName,ScheduleInterval,ScheduleFrequency,EffectiveFrom,EffectiveTo, 
(SELECT FORMAT ( EffectiveFrom, 'MM/dd/yyyy hh:mm tt', 'en-US')) as EffectiveFromString, 
(SELECT FORMAT ( EffectiveTo, 'MM/dd/yyyy hh:mm tt', 'en-US')) as EffectiveToString from Busdta.schedule Where ScheduleID='" + ScheduleID + "'";
                DataSet ScheduleDetail = DbHelper.ExecuteDataSet(query1, "SystemDB");
                if (ScheduleDetail.HasData())
                {
                    EditSchedule = ScheduleDetail.GetEntityList<Service>();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return EditSchedule.FirstOrDefault();
        }

        public bool UpdateSchedule(Service sc)
        {
            string query1 = string.Empty;
            int result = 0;
            List<Service> UpdateSchedule = new List<Service>();
            try
            {
                //{
                DateTime nextrun = new DateTime();
                if (sc.ScheduleFrequency == "Every 20 Mins")
                {
                    nextrun = sc.EffectiveFrom.AddMinutes(20);
                }
                else if (sc.ScheduleFrequency == "Every 1 Hour")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 3 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(3);
                }
                else if (sc.ScheduleFrequency == "Every 12 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(12);
                }

                int target = 0;
                if (sc.ScheduleInterval == "Weekly")
                {
                    switch (sc.ScheduleFrequency)
                    {
                        case "Sunday":
                            target = 0;
                            break;

                        case "Monday":
                            target = 1;
                            break;
                        case "Tuesday":
                            target = 2;
                            break;
                        case "Wednesday":
                            target = 3;
                            break;
                        case "Thursday":
                            target = 4;
                            break;
                        case "Friday":
                            target = 5;
                            break;
                        case "Saturday":
                            target = 6;
                            break;
                    }
                    int start = (int)sc.EffectiveFrom.DayOfWeek;
                    if (target <= start)
                        target += 7;
                    nextrun = DateTime.Now.AddDays(target - start);
                }

                string Query1 = string.Empty;
                //int IsActive = 1;
                //int IsRead = 0;
                Query1 = "UPDATE BUSDTA.Schedule SET EnvironmentName='" + sc.EnvironmentName + "',ScheduleInterval='" + sc.ScheduleInterval + "',ScheduleFrequency='" + sc.ScheduleFrequency + "',EffectiveFrom='" + sc.EffectiveFrom + "',EffectiveTo='" + sc.EffectiveTo + "',NextRunTime='" + nextrun + "',UpdatedDateTime='" + DateTime.Now + "' where ScheduleID='" + sc.ScheduleID + "'";
                result = DbHelper.ExecuteNonQuery(Query1);
                //Query1 = "Insert into BUSDTA.Schedule(JobID,EnvironmentName,ServiceName,ScheduleInterval,ScheduleFrequency,EffectiveFrom,EffectiveTo,IsActive,IsRead,CreatedDateTime,NextRunTime)";
                //Query1 = Query1 + "VALUES('" + sc.JobID + "','" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + sc.ScheduleInterval + "','" + sc.ScheduleFrequency + "','" + sc.EffectiveFrom + "','" + sc.EffectiveTo + "','" + IsActive + "','" + IsRead + "','" + DateTime.Now + "','" + nextrun + "')";
                //result = DbHelper.ExecuteNonQuery(Query1);
                // }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool DeleteEmailConfig(string id)
        {
            string query1 = string.Empty;
            int result = 0;
            List<EmailConfig> EditEmail = new List<EmailConfig>();
            try
            {
                query1 = "DELETE FROM [BUSDTA].[EmailConfig] WHERE ID=" + id;
                result = DbHelper.ExecuteNonQuery(query1);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result > 0 ? true : false;
        }





        #region Save New Schedule
        public bool SaveNewSchedule(Service sc)
        {
            int result = -1;
            try
            {
                DateTime nextrun = new DateTime();
                if (sc.ScheduleFrequency == "Every 20 Mins")
                {
                    nextrun = sc.EffectiveFrom.AddMinutes(20);
                }
                else if (sc.ScheduleFrequency == "Every 1 Hour")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 3 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(3);
                }
                else if (sc.ScheduleFrequency == "Every 12 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(12);
                }

                int target = 0;
                if (sc.ScheduleInterval == "Weekly")
                {
                    switch (sc.ScheduleFrequency)
                    {
                        case "Sunday":
                            target = 0;
                            break;

                        case "Monday":
                            target = 1;
                            break;
                        case "Tuesday":
                            target = 2;
                            break;
                        case "Wednesday":
                            target = 3;
                            break;
                        case "Thursday":
                            target = 4;
                            break;
                        case "Friday":
                            target = 5;
                            break;
                        case "Saturday":
                            target = 6;
                            break;
                    }
                    int start = (int)sc.EffectiveFrom.DayOfWeek;
                    if (target <= start)
                        target += 7;
                    nextrun = DateTime.Now.AddDays(target - start);
                }

                string Query1 = string.Empty;
                int IsActive = 1;
                int IsRead = 0;
                Query1 = "Insert into BUSDTA.Schedule(JobID,EnvironmentName,ServiceName,ScheduleInterval,ScheduleFrequency,EffectiveFrom,EffectiveTo,IsActive,IsRead,CreatedDateTime,NextRunTime)";
                Query1 = Query1 + "VALUES('" + sc.JobID + "','" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + sc.ScheduleInterval + "','" + sc.ScheduleFrequency + "','" + sc.EffectiveFrom + "','" + sc.EffectiveTo + "','" + IsActive + "','" + IsRead + "','" + DateTime.Now + "','" + nextrun + "')";
                result = DbHelper.ExecuteNonQuery(Query1);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result > 0 ? true : false;
        }
        #endregion


        #region Save Immediate Schedule
        public bool SaveImmediateSchedule(Service sc)
        {
            int result = -1;
            try
            {
                

                string Query1 = string.Empty;
                string Query2 = string.Empty;
                string Query3 = string.Empty;
                int IsActive = 1;
                int IsImmediate = 1;
                int IsRead = 0;
                int IsExecuting;
                Query1 = "SELECT count(IsExecuting) from BUSDTA.SCHEDULE Where IsExecuting=1";
                IsExecuting =Convert.ToInt32(DbHelper.ExecuteScalar(Query1));
                if (IsExecuting > 0)
                {
                    Query2 = "Insert into BUSDTA.Schedule(JobID,EnvironmentName,ServiceName,IsImmediate,IsWaiting,IsActive,IsRead,CreatedDateTime,UpdatedDateTime)";
                    Query2 = Query2 + "VALUES('" + sc.JobID + "','" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + IsImmediate + "','1','" + IsActive + "','" + IsRead + "','" + DateTime.Now + "','" + DateTime.Now + "')";
                    result = DbHelper.ExecuteNonQuery(Query2);
                    return false;
                }
                else
                {

                    Query3 = "Insert into BUSDTA.Schedule(JobID,EnvironmentName,ServiceName,IsImmediate,IsActive,IsRead,CreatedDateTime,UpdatedDateTime)";
                    Query3 = Query3 + "VALUES('" + sc.JobID + "','" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + IsImmediate + "','" + IsActive + "','" + IsRead + "','" + DateTime.Now + "','" + DateTime.Now + "')";
                    result = DbHelper.ExecuteNonQuery(Query3);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result > 0 ? true : false;
        }
        #endregion



        public List<EmailGroupDdl> GetEmailGroupDdl()
        {
            List<EmailGroupDdl> lst = new List<EmailGroupDdl>();
            try
            {
                logger.Info("EmailGroup DropDownList");
                string query = string.Empty;
                query = "select ID, GroupEmailName from BUSDTA.EmailConfig";
                DataSet dsEmail = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEmail.HasData())
                {
                    lst = dsEmail.GetEntityList<EmailGroupDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Schedule List
        public List<Service> GetServiceScheduleList()
        {
            try
            {
                string query1 = string.Empty;
                //  query1 = "SELECT * FROM BUSDTA.schedule where JDEinterfaceID LIKE '%" + JDEInterfaceID + "%' and ScheduleInterval LIKE '%" + Mode + "%'  ";
                //  query1 = "SELECT * FROM BUSDTA.schedule where IsImmediate=0";
                query1 = "select * from busdta.schedule sc join busdta.JobDetails jb on sc.JobID=jb.JobID where IsImmediate=0";
                DataSet dsschedule = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<Service> schedule1 = new List<Service>();
                if (dsschedule.HasData())
                {
                    schedule1 = dsschedule.GetEntityList<Service>();
                }

                return schedule1;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        //Immediate List
        public List<ImmediateService> GetImmediateList()
        {
            try
            {
                string query1 = string.Empty;
                //  query1 = "SELECT * FROM BUSDTA.schedule where JDEinterfaceID LIKE '%" + JDEInterfaceID + "%' and ScheduleInterval LIKE '%" + Mode + "%'  ";
                //   query1 = "SELECT * FROM BUSDTA.schedule where IsImmediate=1";
                query1 = "select * from busdta.schedule sc join busdta.JobDetails jb on sc.JobID=jb.JobID where IsImmediate=1";
                DataSet dsschedule = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ImmediateService> schedule1 = new List<ImmediateService>();
                if (dsschedule.HasData())
                {
                    schedule1 = dsschedule.GetEntityList<ImmediateService>();
                }

                return schedule1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //Service List
        public List<ServiceHistory> GetServiceList()
        {
            try
            {
                string query1 = string.Empty;

                // query1 = "select * from busdta.servicehistory sh join busdta.JobDetails jd on sh.JobID=jd.JobID";
                query1 = " select sh.HistoryID,sh.JobID,jd.JobName,sh.EnvironmentName,sh.LastRunDateTime,sh.LastUpdateDateTime from busdta.servicehistory sh join busdta.JobDetails jd on sh.JobID=jd.JobID group by sh.HistoryID,sh.JobID,jd.JobName,sh.EnvironmentName,sh.LastRunDateTime,sh.LastUpdateDateTime";
                DataSet dsservice = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ServiceHistory> servicehistory = new List<ServiceHistory>();
                if (dsservice.HasData())
                {
                    servicehistory = dsservice.GetEntityList<ServiceHistory>();
                }

                return servicehistory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool ServiceStatus()
        {
         // ServiceController svcController = new ServiceController("DTService");
            ServiceController svcController = new ServiceController("DTService", "10.30.206.171");
           if (svcController != null)
            {
                try
                {

                    //if (svcController.Status.ToString() == "Running")
                    //{
                    //    return true;
                    //}
                   // ServiceController svcController = new ServiceController("DTService");
                    using (Impersonation imperson = new Impersonation("fbctorrance", "mobdev3", "torrdev1"))
                    {

                        if (svcController.Status.ToString() == "Running")
                        {
                            return true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
           
            return false;
        }

        //Jobs List
        public List<JobDetails> GetjobDetails()
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT * FROM BUSDTA.JObDetails";
                DataSet dsjobs = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<JobDetails> jobdetail = new List<JobDetails>();
                if (dsjobs.HasData())
                {
                    jobdetail = dsjobs.GetEntityList<JobDetails>();
                }

                return jobdetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<JobDetails> GetJobNameDdl()
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT Jobid,JobName from BUSDTA.JobDetails Where IsActive=1";
                DataSet dsJobNames = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<JobDetails> jobname = new List<JobDetails>();
                if (dsJobNames.HasData())
                {
                    jobname = dsJobNames.GetEntityList<JobDetails>();
                }
                return jobname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Active
        #region GetJobDetailsList
        public List<ModuleSequence> GetJobDetailsList(int JobID)
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT BUSDTA.ModuleSequence.ID , BUSDTA.ModuleSequence.JobID, BUSDTA.ModuleSequence.SequenceNo, BUSDTA.ModuleSequence.ModuleName, BUSDTA.ModuleSequence.Dependency, BUSDTA.ModuleSequence.EmailNotification, BUSDTA.EmailConfig.GroupEmailName as EmailAddress FROM BUSDTA.EmailConfig RIGHT JOIN BUSDTA.ModuleSequence ON BUSDTA.EmailConfig.ID = BUSDTA.ModuleSequence.EmailConfigID where JobID='" + JobID + "' ORDER BY BUSDTA.ModuleSequence.SequenceNo";
                DataSet dsjobdetaillist = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ModuleSequence> joblist = new List<ModuleSequence>();
                if (dsjobdetaillist.HasData())
                {
                    joblist = dsjobdetaillist.GetEntityList<ModuleSequence>();
                }

                return joblist;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion


        //Active
        #region GetScheduleDetailsList
        public List<Service> GetScheduleDetailsList(int ScheduleID)
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT DISTINCT JobName,ScheduleInterval,ScheduleFrequency,EffectiveFrom,EffectiveTo FROM BUSDTA.ModuleSequence ms JOIN BUSDTA.Schedule s on ms.JobID=s.JobID JOIN busdta.jobdetails jd on s.JobID=jd.JobID where s.ScheduleID='" + ScheduleID + "' Group By JobName,ScheduleInterval,ScheduleFrequency,EffectiveFrom,EffectiveTo";
                DataSet dsscheduledetaillist = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<Service> schedulelist = new List<Service>();
                if (dsscheduledetaillist.HasData())
                {
                    schedulelist = dsscheduledetaillist.GetEntityList<Service>();
                }

                return schedulelist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetImmediateScheduleDetailsList
        public List<ImmediateService> GetImmediateDetailsList(int JobID)
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT * FROM BUSDTA.ModuleSequence ms JOIN BUSDTA.Schedule s on ms.JobID=s.JobID where s.JobID='" + JobID + "'";
                DataSet dsscheduledetaillist = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ImmediateService> schedulelist = new List<ImmediateService>();
                if (dsscheduledetaillist.HasData())
                {
                    schedulelist = dsscheduledetaillist.GetEntityList<ImmediateService>();
                }

                return schedulelist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region ServiceHistoryDetailsList
        public List<ModuleSequence> GetServicHistoryDetailsList(int HistoryID)
        {

            try
            {
                string query1 = string.Empty;
                // query1 = "SELECT * FROM BUSDTA.ServiceHistory ms JOIN BUSDTA.Schedule s on ms.JobID=s.JobID where s.JobID='" + JobID + "'";
                //  query1 = "select * from busdta.ServiceHistory sh INNER JOIN BUSDTA.ModuleSequence ms on sh.JobID=ms.JobID and sh.ModuleName=ms.ModuleName INNER JOIN BUSDTA.jobdetails jd on sh.JobID=jd.JobID where sh.HistoryID='" + HistoryID + "'";
                //query1 = "select * from busdta.ServiceHistory sh INNER JOIN BUSDTA.ModuleSequence ms on sh.JobID=ms.JobID INNER JOIN BUSDTA.jobdetails jd on sh.JobID=jd.JobID where sh.HistoryID='" + HistoryID + "'";
                query1 = "SELECT DISTINCT BUSDTA.ServiceHistoryDetail.Seqno as SequenceNo, BUSDTA.ServiceHistoryDetail.ModuleName, BUSDTA.ServiceHistoryDetail.Status, BUSDTA.ServiceHistoryDetail.Dependency, BUSDTA.ServiceHistoryDetail.EmailNotification, BUSDTA.EmailConfig.GroupEmailName FROM  BUSDTA.EmailConfig RIGHT JOIN BUSDTA.ServiceHistoryDetail ON BUSDTA.EmailConfig.ID = BUSDTA.ServiceHistoryDetail.EmailConfigID where SHID='" + HistoryID + "'";
                DataSet dsservicehistorydetails = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ModuleSequence> servicehistorylist = new List<ModuleSequence>();
                if (dsservicehistorydetails.HasData())
                {
                    servicehistorylist = dsservicehistorydetails.GetEntityList<ModuleSequence>();
                }

                return servicehistorylist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion





        public List<ServiceSubModule> GetServiceScheduleList1(string JDEID)
        {
            try
            {
                string query1 = string.Empty;
                query1 = "SELECT * FROM BUSDTA.submoduledetails where JDEInterfaceID='" + JDEID + "'";
                DataSet dsschedule = DbHelper.ExecuteDataSet(query1, "SystemDB");
                List<ServiceSubModule> schedule1 = new List<ServiceSubModule>();
                if (dsschedule.HasData())
                {
                    schedule1 = dsschedule.GetEntityList<ServiceSubModule>();
                }

                return schedule1;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public List<ModuleDdl> GetModuleddl()
        {
            List<ModuleDdl> lst = new List<ModuleDdl>();
            try
            {
                logger.Info("User DropDownList");
                string query = string.Empty;
                query = "select ModuleID, ModuleName from BUSDTA.modulemaster where Isactive=1";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<ModuleDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ModuleDdl> LoadModule()
        {
            List<ModuleDdl> lst = new List<ModuleDdl>();
            try
            {
                logger.Info("User DropDownList");
                string query = string.Empty;
                query = "select  ModuleName from BUSDTA.schedule where Isactive=1";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<ModuleDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      

        public List<JobDetails> GetJobtoEdit(string jodid)
        {
            string query1 = string.Empty;

            List<JobDetails> schedule1 = new List<JobDetails>();
            try
            {
                query1 = " SELECT Job.JobID,JobName,CreatedDate,LastModifiedDate,IsActive,ID,Seq.JobID,SequenceNo,ModuleName,Dependency,EmailNotification,EmailConfigID as EmailAddress FROM  BUSDTA.JobDetails  Job INNER JOIN BUSDTA.ModuleSequence Seq ON Job.JobID = Seq.JobID WHERE Seq.JobID=" + jodid + " AND Job.IsActive=1 ORDER BY Seq.SequenceNo";
                DataSet dsschedule = DbHelper.ExecuteDataSet(query1, "SystemDB");
                if (dsschedule.HasData())
                {
                    schedule1 = dsschedule.GetEntityList<JobDetails>();
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return schedule1;
        }
        public void UpdateActive(string jodid, string active)
        {
            string MyQuery = string.Empty;
            try
            {
                MyQuery = "UPDATE [BUSDTA].[JobDetails] SET [IsActive] =" + active + " WHERE JobID=" + jodid;
                DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Save(Service sc)
        {
            try
            {
                DateTime nextrun = new DateTime();
                if (sc.ScheduleFrequency == "Every 20 Mins")
                {
                    nextrun = sc.EffectiveFrom.AddMinutes(20);
                }
                else if (sc.ScheduleFrequency == "Every 1 Hour")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 3 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 12 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(12);
                }

                int target = 0;
                if (sc.ScheduleInterval == "Weekly")
                {
                    switch (sc.ScheduleFrequency)
                    {
                        case "Sunday":
                            target = 0;
                            break;

                        case "Monday":
                            target = 1;
                            break;
                        case "Tuesday":
                            target = 2;
                            break;
                        case "Wednesday":
                            target = 3;
                            break;
                        case "Thursday":
                            target = 4;
                            break;
                        case "Friday":
                            target = 5;
                            break;
                        case "Saturday":
                            target = 6;
                            break;
                    }
                    int start = (int)sc.EffectiveFrom.DayOfWeek;
                    if (target <= start)
                        target += 7;
                    nextrun = DateTime.Now.AddDays(target - start);
                }


                logger.Info("UserMasterManager SaveUser");
                string query1 = string.Empty;
                query1 = "SELECT count(*) from BUSDTA.schedule where EnvironmentName='" + sc.EnvironmentName + "' and IsActive=1 and IsWaiting=0";
                int IsActive = Convert.ToInt32(DbHelper.ExecuteScalar(query1).ToString());
                string query2 = string.Empty;
                query2 = "SELECT count(*) from BUSDTA.schedule where EnvironmentName='" + sc.EnvironmentName + "' and  IsWaiting=1";
                int IsWaiting = Convert.ToInt32(DbHelper.ExecuteScalar(query2).ToString());
                string query6 = string.Empty;


                query6 = "SELECT count(*) from BUSDTA.schedule where EnvironmentName='" + sc.EnvironmentName + "' and  IsExecuting=1";
                int IsExecuting = Convert.ToInt32(DbHelper.ExecuteScalar(query6).ToString());
                if (IsActive > 0 && IsWaiting == 0)
                {
                    string query3 = string.Empty;
                    query3 = "UPDATE BUSDTA.schedule SET Isactive=0 where EnvironmentName='" + sc.EnvironmentName + "' and Isactive=1";
                    DbHelper.ExecuteNonQuery(query3, null, false, "SystemDB");
                    sc.IsRead = false;
                    string query4 = string.Empty;

                    query4 = "INSERT INTO BUSDTA.schedule([EnvironmentName],[ServiceName],[ScheduleInterval],[Schedulefrequency],[EffectiveFrom],[EffectiveTo],[ScheduleStatus],[IsRead],[CreatedDateTime],[UpdatedDateTime],[NextRunTime]) values('" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + sc.ScheduleInterval + "','" + sc.ScheduleFrequency + "','" + sc.EffectiveFrom + "','" + sc.EffectiveTo + "','" + sc.ScheduleStatus + "','" + sc.IsRead + "','" + DateTime.Now + "','" + DateTime.Now + "','" + nextrun + "')";
                    int result = DbHelper.ExecuteNonQuery(query4, null, false, "SystemDB");
                    return result > 0 ? true : false;
                }
                if (IsWaiting == 1)
                {
                    return false;
                }
                if (IsExecuting == 1)
                {
                    string query7 = string.Empty;
                    query7 = "UPDATE BUSDTA.schedule SET IsWaiting=1 where EnvironmentName='" + sc.EnvironmentName + "' and IsExecuting=1 and Isactive=1";
                    DbHelper.ExecuteNonQuery(query7, null, false, "SystemDB");
                }

                sc.IsRead = false;

                string query5 = string.Empty;

                query5 = "INSERT INTO BUSDTA.schedule([ModuleName],[EnvironmentName],[ServiceName],[ScheduleInterval],[Schedulefrequency],[EffectiveFrom],[EffectiveTo],[ScheduleStatus],[IsRead],[CreatedDateTime],[UpdatedDateTime],[NextRunTime]) values('" + sc.ModuleName + "','" + sc.EnvironmentName + "','" + sc.ServiceName + "','" + sc.ScheduleInterval + "','" + sc.ScheduleFrequency + "','" + sc.EffectiveFrom + "','" + sc.EffectiveTo + "','" + sc.ScheduleStatus + "','" + sc.IsRead + "','" + DateTime.Now + "','" + DateTime.Now + "','" + nextrun + "')";
                int result1 = DbHelper.ExecuteNonQuery(query5, null, false, "SystemDB");
                return result1 > 0 ? true : false;


            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public Service Edit(string JDEInterfaceID)
        {
            try
            {
                string query = string.Empty;
                query = "SELECT * FROM BUSDTA.schedule WHERE JDEInterfaceID='" + JDEInterfaceID + "' and IsActive=1";
                DataSet ds = DbHelper.ExecuteDataSet(query, "SystemDB");
                Service service = new Service();
                if (ds.HasData())
                {
                    service = ds.GetEntity<Service>();
                }
                return service;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool UpdateServiceSchedule(Service sc)
        {
            try
            {
                logger.Info("Service Schedule Update");
                string query = string.Empty;

                DateTime nextrun = new DateTime();
                if (sc.ScheduleFrequency == "Every 20 Mins")
                {
                    nextrun = sc.EffectiveFrom.AddMinutes(20);
                }
                else if (sc.ScheduleFrequency == "Every 1 Hour")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 3 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(1);
                }
                else if (sc.ScheduleFrequency == "Every 12 Hours")
                {
                    nextrun = sc.EffectiveFrom.AddHours(12);
                }

                int target = 0;
                if (sc.ScheduleInterval == "Weekly")
                {
                    switch (sc.ScheduleFrequency)
                    {
                        case "Sunday":
                            target = 0;
                            break;
                        case "Monday":
                            target = 1;
                            break;
                        case "Tuesday":
                            target = 2;
                            break;
                        case "Wednesday":
                            target = 3;
                            break;
                        case "Thursday":
                            target = 4;
                            break;
                        case "Friday":
                            target = 5;
                            break;
                        case "Saturday":
                            target = 6;
                            break;
                    }
                    int start = (int)sc.EffectiveFrom.DayOfWeek;
                    if (target <= start)
                        target += 7;
                    nextrun = DateTime.Now.AddDays(target - start);
                }



                query = "UPDATE BUSDTA.schedule SET ScheduleInterval='" + sc.ScheduleInterval + "',ScheduleFrequency='" + sc.ScheduleFrequency + "',EffectiveFrom='" + sc.EffectiveFrom + "',EffectiveTo='" + sc.EffectiveTo + "',NextRunTime='" + nextrun + "',UpdatedDateTime='" + DateTime.Now + "' where EnvironmentName='" + sc.EnvironmentName + "' and IsActive=1";

                //  query = string.Format(query, sc.ScheduleInterval, sc.ScheduleFrequency, sc.EffectiveFrom, sc.EffectiveTo, DateTime.Now, "1", null, "0");
                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Service Schedule Update" + ex.Message);
                throw ex;
            }
        }

        public int ExecutionStatus()
        {
            try
            {
                string Query = "select count(*) from BUSDTA.schedule where IsExecuting=1";
                int result = Convert.ToInt32(DbHelper.ExecuteScalar(Query));
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }


        public int UpdateWaiting()
        {
            try
            {
                string Query = "update BUSDTA.schedule set waiting=1 where IsExecuting=1";
                int result = Convert.ToInt32(DbHelper.ExecuteScalar(Query));
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
    }
}