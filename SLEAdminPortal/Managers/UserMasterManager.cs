﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Data;
using SLEAdminPortal.Models;
using System.Web.Mvc.Properties;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;


namespace SLEAdminPortal.Managers
{
    public class UserMasterManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //List all the Users in UserMaster and for Search the Users
        public List<User> GetUserList(string UserID, string FirstName, string DomainUser)
        {
            try
            {
                logger.Info("UserMasterManager GetUserList");
                string query = string.Empty;

                query = "SELECT um.NTUser,um.App_User_ID,um.DisplayName,um.Email,um.Phone,um.Domain,um.Active, " +
                        " STUFF((SELECT ', ' + Convert(varchar(100),EM.EnvironmentMasterId)  " +
                        " FROM BUSDTA.User_Environment_Map uem " +
                        " LEFT JOIN BUSDTA.Environment_Master EM  ON EM.EnvironmentMasterId = uem.EnvironmentMasterId " +
                        " WHERE uem.AppUserId = um.App_user_id FOR XML PATH('')), 1, 2, '') AS 'EnvironmentId', " +
                        " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.User_Environment_Map uem  " +
                        " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = uem.EnvironmentMasterId " +
                        " WHERE uem.AppUserId = um.App_user_id FOR XML PATH('')), 1, 2, '') AS 'Environment' " +
                        " FROM BUSDTA.UserProfileMaster um WHERE 1=1";

//                query = @"SELECT um.NTUser,um.App_User_ID,um.DisplayName AS 'DisplayName',um.Email,um.Phone,um.Domain,um.Active,  
//STUFF((SELECT ', ' + Convert(varchar(100),EM.EnvironmentMasterId)   
//FROM BUSDTA.User_Environment_Map uem  
//LEFT JOIN BUSDTA.Environment_Master EM  ON EM.EnvironmentMasterId = uem.EnvironmentMasterId  
//WHERE uem.AppUserId = um.App_user_id FOR XML PATH('')), 1, 2, '') AS 'EnvironmentId',  
//STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.User_Environment_Map uem   
//LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = uem.EnvironmentMasterId  
//WHERE uem.AppUserId = um.App_user_id FOR XML PATH('')), 1, 2, '') AS 'Environment' 
//FROM BUSDTA.UserProfileMaster um WHERE 1=1";

                if (string.IsNullOrEmpty(UserID) != true && UserID.Trim() != "")
                    query += " AND NTUser LIKE '%" + UserID + "%'";
                if (string.IsNullOrEmpty(FirstName) != true && FirstName.Trim() != "")
                    query += " AND DisplayName LIKE '%" + FirstName + "%'";
                if (string.IsNullOrEmpty(DomainUser) != true && DomainUser.Trim() != "")
                    query += " AND Domain LIKE '%" + DomainUser + "%'";
                DataSet dsUser = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<User> UsersList = new List<User>();
                if (dsUser.HasData())
                {
                    UsersList = dsUser.GetEntityList<User>();
                }

                //List<User> newLst = new List<User>();
                //foreach (var i in UsersList)
                //{
                //    newLst.Add(new User
                //    {
                //        Active=i.Active,
                //        App_user_id=i.App_user_id,
                //        DisplayName=i.DisplayName,
                //        Domain=i.Domain,
                //        Email=i.Email,
                //        Environment=i.Environment,
                //        EnvironmentId=i.EnvironmentId,
                //        NTUser=i.NTUser,
                //        Phone=i.Phone,
                //        VersionNumber = getVersionNumber(i.App_user_id, i.EnvironmentId),
                //        ReleasedDate = getReleasedDate(i.App_user_id, i.EnvironmentId)
                //    });
                //}


                return UsersList;
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager GetUserList" + ex.Message);
                throw ex;
            }

        }

        //private string getVersionNumber(int App_user_id, string EnvId)
        //{
        //    try
        //    {
        //        string str = null;
        //        string query1 = string.Empty;

        //        string[] arrOldEnv = EnvId.Split(',');
        //        foreach (var i in arrOldEnv)
        //        {
        //            if (!string.IsNullOrEmpty(i))
        //            {
        //                query1 = "select RouteID, ActiveYN from BUSDTA.RemoteDB_Master where DeviceID='" + deviceid + "' and EnvironmentMasterID=" + Convert.ToInt32(i);
        //                DataSet ds1 = DbHelper.ExecuteDataSet(query1, "SystemDB");
        //                RemoteDbNew lst1 = new RemoteDbNew();
        //                if (ds1.HasData())
        //                {
        //                    lst1 = ds1.GetEntity<RemoteDbNew>();
        //                }
        //                if (lst1 == null)
        //                {
        //                    str += str == null ? "Ready To Onboard" : ",Ready To Onboard";
        //                }
        //                else
        //                {
        //                    str += str == null ? lst1.ActiveYN == "Y" ? "Onboarded" : lst1.ActiveYN == "Y" ? "Reserved" : "Ready To Onboard" : lst1.ActiveYN == "Y" ? ",Onboarded" : lst1.ActiveYN == "Y" ? ",Reserved" : ",Ready To Onboard";
        //                }
        //            }
        //        }

        //        return str;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}


        


        //Add New User 
       
        public List<User> SaveUser(string NTUser, string FirstName, string LastName, string DisplayName, string Title, string Domain, string Email, string Phone)
        {
            try
            {
                logger.Info("UserMasterManager SaveUser");
                string query1 = string.Empty;
                query1 += "IF NOT EXISTS (SELECT * FROM BUSDTA.UserProfileMaster d where UPPER(d.NTUser) = '" + NTUser.ToUpper() + "'  and Upper(d.Domain)='" + Domain.ToUpper() + "')";
                query1 += "INSERT INTO busdta.UserProfileMaster([NTUser],[FirstName],[LastName],[DisplayName],[Title],[Domain],[Email],[Phone],[Active]) values('" + NTUser + "','" + FirstName + "','" + LastName + "','" + DisplayName + "','" + Title + "','" + Domain + "','" + Email + "','" + Phone + "',1)";
                DataSet dsUser = DbHelper.ExecuteDataSet(query1, "SystemDB");
                return GetUserList(null, null, null);
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager SaveUser" + ex.Message);
                throw ex;
            }
        }

        //Create Popup and get the values of User to be edit
        public User GetUserForEdit(int App_user_id)
        {
            try
            {
                logger.Info("UserMasterManager EditUser");
                string query = string.Empty;
                query = "SELECT App_user_id,NTUser,FirstName,LastName,DisplayName,Title,Domain,Email,Phone  FROM busdta.UserProfileMaster WHERE App_User_Id='" + App_user_id + "'";
                DataSet dsUser = DbHelper.ExecuteDataSet(query, "SystemDB");
                User Usersobj = new User();
                if (dsUser.HasData())
                {
                    Usersobj = dsUser.GetEntity<User>();
                }
                return Usersobj;
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager EditUser" + ex.Message);
                throw ex;
            }

        }

        //Update the Changes of User
        public User UpdateUser(User model)
        {
            try
            {
                logger.Info("UserMasterManager UpdateUser");
                string query = string.Empty;
                query = "Update busdta.UserProfileMaster SET NTUser='" + model.NTUser + "',FirstName='" + model.FirstName + "',LastName='" + model.LastName + "',DisplayName='" + model.DisplayName + "',Title='" + model.Title + "',Domain='" + model.Domain + "',Email='" + model.Email + "',Phone='" + model.Phone + "' WHERE App_user_id='" + model.App_user_id + "' ";
                DataSet dsUser = DbHelper.ExecuteDataSet(query, "SystemDB");
                User Usersobj = new User();
                if (dsUser.HasData())
                {
                    Usersobj = dsUser.GetEntity<User>();
                }
                return Usersobj;
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager UpdateUser" + ex.Message);
                throw ex;
            }

        }

        //To Delete the Particular User 
        public List<User> DeleteUser(int App_user_id)
        {
            try
            {
                logger.Info(" UserMasterManager DeleteUser");
                string query = string.Empty;
                query = query + "Delete from Busdta.UserProfileMaster where App_User_Id=" + App_user_id;
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<User> User1 = new List<User>();
                if (dsEnv.HasData())
                {
                    User1 = dsEnv.GetEntityList<User>();
                }
                return User1;

            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager DeleteUser" + ex.Message);
                throw ex;
            }
        }


        //For checking duplicate
        public string CheckForDuplicateUserID(string App_user)
        {
            try
            {
                logger.Info(" UserMasterManager CheckForDuplicateUserID");
                string query = string.Empty;
                query = "SELECT count(*) FROM BUSDTA.UserProfileMaster d where d.NTUser = '" + App_user + "'";
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                string count = string.Empty;
                if (dsEnv.HasData())
                {
                    count = dsEnv.Tables[0].Rows[0][0].ToString();
                }
                
                return count;

            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager DeleteUser" + ex.Message);
                throw ex;
            }
        }

        public string CheckForDuplicateEmail(string Email)
        {
            try
            {
                logger.Info(" UserMasterManager CheckForDuplicateDomainUser");
                string query = string.Empty;
                query = "SELECT count(*) FROM BUSDTA.UserProfileMaster d where d.Email = '" + Email + "'";
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                string count = string.Empty;
                if (dsEnv.HasData())
                {
                    count = dsEnv.Tables[0].Rows[0][0].ToString();
                }
                return count;

            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager CheckForDuplicateDomainUser" + ex.Message);
                throw ex;
            }
        }


        //To change User from active to inactive or vice versa
        public bool ToggleUpdate(string Active, string App_User)
        {
            int result = -1;
            try
            {
                logger.Info("UserMasterManager ToggleActive");
                string MyQuery = string.Empty;
                int toggleTo = Active.Equals("0") ? 1 : 0;
                MyQuery = "UPDATE BUSDTA.UserProfileMaster SET Active=" + toggleTo + " WHERE NTUser='" + App_User + "'";
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserMasterManager ToggleActive" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }


        // User Environment Mapping Development

        #region User Environment

        public string GetEnvironmentByUserID(string AppUserID)
        {
            try
            {
                logger.Info("User Environment Mapping Save");
                string query = string.Empty;

                query = " select STUFF((SELECT ', ' + CONVERT(varchar(100), EM.EnvironmentMasterId) FROM BUSDTA.User_Environment_Map UEM  LEFT JOIN BUSDTA.Environment_Master EM  ON EM.EnvironmentMasterId = UEM.EnvironmentMasterId " +
                        " WHERE UEM.AppUserId = '" + AppUserID + "' FOR XML PATH('')), 1, 2, '') AS 'EnvironmentId'";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");

                return dsEnv.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment Mapping Save" + ex.Message);
                throw ex;
            }
        }

        public bool SaveUserEnvironmentMapping(int EnvironmentMasterId, string AppUserId)
        {
            try
            {
                logger.Info("User Environment Mapping Save");
                string query = string.Empty;
                int UserEnvironmentMapId = 0;
                query = "  SELECT Isnull(MAX(UserEnvironmentMapId)+1,1) FROM BUSDTA.User_Environment_Map";
                UserEnvironmentMapId = Convert.ToInt32(DbHelper.ExecuteScalar(query));

                UserEnvironmentMapId = (UserEnvironmentMapId < 0) ? 1 : UserEnvironmentMapId;

                query = "IF NOT EXISTS (SELECT AppUserId FROM BUSDTA.User_Environment_Map WHERE AppUserId='" + AppUserId + "' AND EnvironmentMasterId=" + EnvironmentMasterId + ")" +
                        " INSERT INTO BUSDTA.User_Environment_Map VALUES(" + UserEnvironmentMapId + "," + EnvironmentMasterId + ",'" + AppUserId + "',1,1,GETDATE(),null,null)";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in User Environment Mapping Save" + ex.Message);
                throw ex;
            }
        }

        public bool DeleteUserEnvironmentMapping(string AppUserId, int EnvironmentMasterId)
        {
            try
            {
                logger.Info("User Environment Mapping Delete");
                string query = string.Empty;

                query = "DELETE FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=" + EnvironmentMasterId + " AND AppUserId='" + AppUserId + "'";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in User DeviceEnvironmentMapping Delete" + ex.Message);
                throw ex;
            }
        }

        #endregion
    }
}