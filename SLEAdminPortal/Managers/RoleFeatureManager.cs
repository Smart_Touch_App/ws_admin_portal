﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEAdminPortal.Models;
using SLEAdminPortal.Helpers;
using System.Text;
using System.Data;
using System.Web.Mvc;

namespace SLEAdminPortal.Managers
{
    public class RoleFeatureManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<RoleFeature> GetRoleFeatureList()
        {
            try
            {
                logger.Info("SecurableFeatureManager GetSecurableFeatureList");
                StringBuilder MyRoleFeatureQuery = new StringBuilder();
                MyRoleFeatureQuery.Append("SELECT rm.RoleName , rm.RoleID,sf.FeatureName,sf.FeatureID,rf.SettingValue, sf.OptionValue FROM BUSDTA.RoleFeatures rf ");
                MyRoleFeatureQuery.Append("INNER JOIN BUSDTA.RoleMaster rm ON rf.RoleID=rm.RoleID ");
                MyRoleFeatureQuery.Append("INNER JOIN BUSDTA.SecurableFeatures sf ON rf.FeatureID = sf.FeatureID order by rm.RoleName asc");
                DataSet dsSecurableFeature = DbHelper.ExecuteDataSet(MyRoleFeatureQuery.ToString(), "SystemDB");
                List<RoleFeature> lstRoleFeature = new List<RoleFeature>();
                if (dsSecurableFeature.HasData())
                {
                    lstRoleFeature = dsSecurableFeature.GetEntityList<RoleFeature>();
                }
                return lstRoleFeature;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<RoleFeature> GetSpecificRoleFeatureList(string RoleName, string FeatureName)
        {         
            string query = string.Empty;
            try
            {
                logger.Info("RoleFeatureManager GetSpecificRoleFeatureList");
                StringBuilder MyRoleFeatureQuery = new StringBuilder();
                MyRoleFeatureQuery.Append("SELECT rm.RoleName , rm.RoleID,sf.FeatureName,sf.FeatureID,rf.SettingValue, sf.OptionValue FROM BUSDTA.RoleFeatures rf ");
                MyRoleFeatureQuery.Append("INNER JOIN BUSDTA.RoleMaster rm ON rf.RoleID=rm.RoleID ");
                MyRoleFeatureQuery.Append("INNER JOIN BUSDTA.SecurableFeatures sf ON rf.FeatureID = sf.FeatureID WHERE 1=1");

                if (string.IsNullOrEmpty(RoleName) != true && RoleName.Trim() != "")
                    MyRoleFeatureQuery.Append("AND rm.RoleName LIKE '%" + RoleName + "%'");

                if (string.IsNullOrEmpty(FeatureName) != true && FeatureName.Trim() != "")
                    MyRoleFeatureQuery.Append("AND sf.FeatureName LIKE '%" + FeatureName + "%'");
                MyRoleFeatureQuery.Append("ORDER BY rm.RoleName");

                DataSet dsRoleFeature = DbHelper.ExecuteDataSet(MyRoleFeatureQuery.ToString(), "SystemDB");
                List<RoleFeature> lstRoleFeature = new List<RoleFeature>();
                if (dsRoleFeature.HasData())
                {
                    lstRoleFeature = dsRoleFeature.GetEntityList<RoleFeature>();
                }
                return lstRoleFeature;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeature GetRoleSelectableList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetRoleSelectableList()
        {
            List<SelectListItem> lstRole = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RoleFeatureManager GetRoleSelectableList");
                StringBuilder MyQuery = new StringBuilder();
                MyQuery.Append("SELECT rm.RoleID,rm.RoleName FROM BUSDTA.RoleMaster rm WITH(NOLOCK)");
                DataSet dsRoleFeature = DbHelper.ExecuteDataSet(MyQuery.ToString(), "SystemDB");
                List<RoleFeature> lstRoleFeature = new List<RoleFeature>();
                if (dsRoleFeature.HasData())
                {
                    foreach (DataRow route in dsRoleFeature.Tables[0].Rows)
                    {
                        lstRole.Add(new SelectListItem { Text = route["RoleName"].ToString(), Value = route["RoleID"].ToString() });
                    }
                }
                return new SelectList(lstRole, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeature GetRoleSelectableList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetFeatureSelectableList()
        {
            List<SelectListItem> lstFeature = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RoleFeatureManager GetFeatureSelectableList");
                StringBuilder MyQuery = new StringBuilder();
                MyQuery.Append("SELECT sf.FeatureID,sf.FeatureName FROM BUSDTA.SecurableFeatures sf WITH(NOLOCK)");
                DataSet dsRoleFeature = DbHelper.ExecuteDataSet(MyQuery.ToString(), "SystemDB");
                List<RoleFeature> lstRoleFeature = new List<RoleFeature>();
                if (dsRoleFeature.HasData())
                {
                    foreach (DataRow route in dsRoleFeature.Tables[0].Rows)
                    {
                        lstFeature.Add(new SelectListItem { Text = route["FeatureName"].ToString(), Value = route["FeatureID"].ToString() });
                    }
                }
                return new SelectList(lstFeature, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeature GetRoleSelectableList" + ex.Message);
                throw ex;
            }
        }

        public bool InsertNewRoleFeature(RoleFeaturesModel model)
        {
            string MyQuery = string.Empty;
            int result = -1;
            try
            {
                MyQuery = "SELECT COUNT(*) FROM BUSDTA.RoleFeatures rf WHERE rf.RoleID='{0}' AND rf.FeatureID='{1}'";
                MyQuery = string.Format(MyQuery, model.SelectedRoleUser, model.SelectedFeature);
                result = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
                if (result == 0)
                {
                    MyQuery = "INSERT INTO BUSDTA.RoleFeatures(RoleID,FeatureID,SettingValue,CreatedBy,CreatedDate) VALUES('{0}','{1}',1,1,GETDATE())";
                    MyQuery = string.Format(MyQuery, model.SelectedRoleUser, model.SelectedFeature);
                    result = DbHelper.ExecuteNonQuery(MyQuery);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result > 0 ? true : false;
        }


        public bool UpdateRoleFeature(string state, string roleID, string featureID)
        {
            int result = -1;
            try
            {
                logger.Info("RoleManager UpdateDevice Parameters: " + state + " " + roleID);
                string MyQuery = string.Empty;
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                MyQuery = "UPDATE BUSDTA.RoleFeatures SET SettingValue=" + toggleTo + " WHERE RoleID=" + roleID + " and featureID = " + featureID;
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeatureManager UpdateDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public DataSet GetOptionType(int featureId)
        {
            try
            {
                logger.Info("RoleFeatureManager GetOptionType Parameters: " + featureId);
                string query = "Select OptionType, OptionValue, DefaultValue from busdta.SecurableFeatures where featureId = " + featureId;
                return DbHelper.ExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeatureManager GetOptionType" + ex.Message);
                throw ex;
            }
        }

        public void SaveRoleFeature(int roleId, int featureId, string settingValue)
        {
            try
            {                
                logger.Info("RoleManager SaveRoleFeature Parameters: " + roleId + " " + featureId + " " + settingValue);
                string MyQuery = string.Empty;
               
                MyQuery = "insert into BUSDTA.RoleFeatures (RoleId, FeatureID, SettingValue, CreatedDate, CreatedBy) values (" + roleId + "," + featureId + ",'" + settingValue + "', '" + DateTime.Now + "', 1)";
                DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeatureManager SaveRoleFeature" + ex.Message);
                throw ex;
            }
        }

        public void DeleteRoleFeature(int roleID, int featureID)
        {
            try
            {
                logger.Info("RoleManager DeleteRoleFeature Parameters: " + roleID + " " + featureID);
                string MyQuery = string.Empty;

                MyQuery = "Delete from BUSDTA.RoleFeatures where RoleID =" + roleID + " and FeatureID=" + featureID;
                DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeatureManager DeleteRoleFeature" + ex.Message);
                throw ex;
            }
        }

        public void SaveRoleFeatureList(int roleId, int featureId, string settingValue)
        {
            try
            {
                char[] trimChar = { '|' };
                settingValue = settingValue.Trim().TrimStart(trimChar);
                logger.Info("RoleManager EditRoleFeatureList Parameters: " + roleId + " " + featureId + " " + settingValue);
                string MyQuery = string.Empty;

                MyQuery = "update BUSDTA.RoleFeatures set SettingValue='" + settingValue + "' where RoleID=" + roleId + " and FeatureID=" + featureId;
                DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeatureManager EditRoleFeatureList" + ex.Message);
                throw ex;
            }
        }
    }
}