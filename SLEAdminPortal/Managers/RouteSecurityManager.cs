﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLEAdminPortal.Helpers;
using System.Web;
using SLEAdminPortal.Models;
using System.Web.Mvc;
using System.Data;
using log4net;
using System.Data.SqlClient;
using System.Configuration;

namespace SLEAdminPortal.Managers
{
    public class RouteSecurityManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<RouteSecurity> GetRouteSecurityList(int envid, string routename, string username)
        {
            try
            {
                logger.Info("Route Security GetRouteSecurityList");
                string query = string.Empty;

                query = " declare @db varchar(150) ";
                query = query + " select @db = cdbaddress from busdta.environment_master where environmentmasterid= " + envid + " ";
                query = query + " declare @temproutemaster table( ";
                query = query + " routemasterid numeric(8,0), ";
                query = query + " routename varchar(150) ";
                query = query + " ) ";
                query = query + " insert into @temproutemaster ";
                query = query + " exec('use ' + @db + ';select routemasterid , routename from busdta.route_master') ";
                query = query + " declare @temproutesecurity table( ";
                query = query + "  ";
                query = query + " routeid int, userid int, roleid int, effectivefrom date, effectiveto date ) ";
                query = query + " insert into @temproutesecurity ";
                query = query + " exec('use ' + @db + ';select routeid, userid, roleid, effectivefrom, effectiveto from busdta.routesecurity') ";
                query = query + " select a.routemasterid, a.routename, b.app_user_id, b.Domain + '\\' + b.NTUSER as App_User, c.roleid, ";
                query = query + " c.rolename, d.effectivefrom, d.effectiveto  from @temproutemaster a, ";
                query = query + " busdta.userprofilemaster b, busdta.rolemaster c, @temproutesecurity d  where a.routemasterid=d.routeid and b.app_user_id=d.userid and c.roleid=d.roleid ";

                if (string.IsNullOrEmpty(routename) != true && routename.Trim() != "")
                {
                    query = query + " and a.routename LIKE '%" + routename + "%' ";
                }
                if (string.IsNullOrEmpty(username) != true && username.Trim() != "")
                {
                    query = query + " and (b.Domain + '\\' + b.NTUSER LIKE '%" + username + "%')";
                }
                query = query + "order by a.routename";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<RouteSecurity> routeSecurity = new List<RouteSecurity>();
                if (dsEnv.HasData())
                {
                    routeSecurity = dsEnv.GetEntityList<RouteSecurity>();
                }
                return routeSecurity;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Route Security GetRouteSecurityList" + ex.Message);
                throw ex;
            }
        }


        public RouteSecurity GetByRouteSecurityId(int RouteId, int RoleId, int UserId, int envid)
        {
            try
            {
                logger.Info("Route Security Get By Route Security Id");
                string query = string.Empty;

                query = " declare @Db varchar(150) " +
                        " select @Db = CDBAddress from BUSDTA.Environment_Master where EnvironmentMasterId=" + envid +
                        " declare @TempRouteMaster table( " +
                        " RouteMasterID numeric(8,0), " +
                        " RouteName varchar(150) " +
                        " ) " +
                        " insert into @TempRouteMaster " +
                        " EXEC('USE ' + @Db + ';SELECT RouteMasterID , RouteName from BUSDTA.Route_Master') " +
                        " declare @TempRouteSecurity table( " +
                        " " +
                        " RouteId int, UserId int, RoleId int, EffectiveFrom date, EffectiveTo date ) " +
                        " insert into @TempRouteSecurity " +
                        " EXEC('USE ' + @Db + ';SELECT RouteId, UserId, RoleId, EffectiveFrom, EffectiveTo from BUSDTA.RouteSecurity') " +
                        " select a.RouteMasterID, a.RouteName, b.App_user_id, b.Domain + '\\' + NTUSER as App_User, c.RoleID, " +
                        " c.RoleName, d.EffectiveFrom, d.EffectiveTo  from @TempRouteMaster a, " +
                        " BUSDTA.userprofilemaster b, BUSDTA.RoleMaster c, @TempRouteSecurity d  where a.RouteMasterID=d.RouteId and b.App_user_id=d.UserId and c.RoleID=d.RoleId " +
                        " and d.RouteId=" + RouteId + " and d.RoleId=" + RoleId + " and UserId=" + UserId;


                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                RouteSecurity routeSecurity = new RouteSecurity();
                if (dsEnv.HasData())
                {
                    routeSecurity = dsEnv.GetEntity<RouteSecurity>();
                }
                return routeSecurity;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteSecurity GetByRouteSecurityId" + ex.Message);
                throw ex;
            }
        }

        public bool SaveRouteSecurity(RouteSecurity rs)
        {
            try
            {
                logger.Info("RouteSecurity Save");
                string query = string.Empty;                        

                query = " declare @Db varchar(150) " +
                        " select @Db = CDBAddress from BUSDTA.Environment_Master where EnvironmentMasterId=" + rs.EnvId +
                        " EXEC('USE ' + @Db + ';Insert into BUSDTA.RouteSecurity VALUES({0},{1},{2},''{3}'',''{4}'',''{5}'',''{6}'',''{7}'',''{8}'', GETDATE()) ')";

                query = string.Format(query, rs.RouteMasterID, rs.App_user_id, rs.RoleID, rs.EffectiveFrom, rs.EffectiveTo, DateTime.Now, "1", null, "0");
                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteSecurity Save" + ex.Message);
                throw ex;
            }
        }

        public bool EditRouteSecurity(RouteSecurity rs)
        {
            try
            {
                logger.Info("RouteSecurity Edit");
                string query = string.Empty;
               
                    query = " declare @Db varchar(150) " +
                            " select @Db = CDBAddress from BUSDTA.Environment_Master where EnvironmentMasterId=" + rs.EnvId +
                            " EXEC('USE ' + @Db + ';update BUSDTA.RouteSecurity set [RouteId] =" + rs.RouteMasterID + ", [UserId]=" + rs.App_user_id + ", [RoleId]=" + rs.RoleID + ",[EffectiveFrom]=''" + rs.EffectiveFrom + "'',[EffectiveTo]=''" + rs.EffectiveTo + "'',[ModifiedBy]=''1''" + ",[ModifiedDate]= GETDATE() where RouteId=" + rs.EditRouteId + " and RoleId=" + rs.EditRoleId + " and UserId=" + rs.EditUserId + "')";
               
                query = string.Format(query, rs.RouteMasterID, rs.App_user_id, rs.RoleID, rs.EffectiveFrom, rs.EffectiveTo, DateTime.Now, "1", null, "0");
                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteSecurity Edit" + ex.Message);
                throw ex;
            }
        }

        public bool DeleteRouteSecurity(int RouteId, int RoleId, int UserId, int EnvId)
        {
            try
            {
                logger.Info("RouteSecurity Delete");
                string query = string.Empty;

                query = " declare @Db varchar(150) " +
                        " select @Db = CDBAddress from BUSDTA.Environment_Master where EnvironmentMasterId=" + EnvId +
                        " EXEC('USE ' + @Db + ';delete from BUSDTA.RouteSecurity where RouteId=" + RouteId + " and RoleId=" + RoleId + " and UserId=" + UserId + " ')";

                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteSecurity Delete" + ex.Message);
                throw ex;
            }
        }

        // User Drop Down List
        public List<UserDdl> GetUserDdl()
        {
            List<UserDdl> lst = new List<UserDdl>();
            try
            {
                logger.Info("User DropDownList");
                string query = string.Empty;
                query = "select App_user_id, Domain + '\\' + NTUSER as App_User from BUSDTA.userprofilemaster where active=1";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<UserDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in User DropDownList" + ex.Message);
                return lst;
            }
        }

        // Role Drop Down List
        public List<RoleDdl> GetRoleDdl()
        {
            List<RoleDdl> lst = new List<RoleDdl>();
            try
            {
                logger.Info("Role DropDownList");
                string query = string.Empty;
                query = "select RoleId, RoleName from BUSDTA.RoleMaster where active=1";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<RoleDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Role DropDownList" + ex.Message);
                return lst;
            }
        }

        // Route Drop Down List
        public List<RouteDdl> GetRouteDdl(int EnvId)
        {
            List<RouteDdl> lst = new List<RouteDdl>();
            try
            {
                logger.Info("Role DropDownList");
                string query = string.Empty;

                query = " declare @Db varchar(150) " +
                        " select @Db = CDBAddress from BUSDTA.Environment_Master where EnvironmentMasterId=" + EnvId +
                        " EXEC('USE ' + @Db + ';select RouteMasterID,RouteName from BUSDTA.Route_Master')";

                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<RouteDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Role DropDownList" + ex.Message);
                return lst;
            }
        }

        // Environment DropDown List

        public List<EnvDdl> GetEnvDdl()
        {
            List<EnvDdl> lst = new List<EnvDdl>();
            try
            {
                logger.Info("Environment DropDownList");
                string query = string.Empty;
                query = "select EnvironmentMasterId, EnvName from BUSDTA.Environment_Master where IsActive=1";
                DataSet dsEnv = DbHelper.ExecuteDataSet(query, "SystemDB");
                if (dsEnv.HasData())
                {
                    lst = dsEnv.GetEntityList<EnvDdl>();
                }
                return lst;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Environment DropDownList" + ex.Message);
                return lst;
            }
        }


    }
}