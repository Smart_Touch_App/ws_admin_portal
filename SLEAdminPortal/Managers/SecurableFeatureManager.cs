﻿//************************************************************************************************
// Comment: Securable Feature Manager : Business Logics
// Created: Mar 20, 2016
// Author: TechUnison India (Velmani Karnan)
//*************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Web.Mvc;
using SLEAdminPortal.Helpers;
using SLEAdminPortal.Models;

namespace SLEAdminPortal.Managers
{
    public class SecurableFeatureManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<SecurableFeature> GetSecurableFeatureList()
        {
            try
            {
                logger.Info("SecurableFeatureManager GetSecurableFeatureList");
                StringBuilder MylstSecurableFeatureQuery = new StringBuilder();
                MylstSecurableFeatureQuery.Append("SELECT FeatureID, FeatureName, FeatureDescription, DefaultValue, OptionType, OptionValue, FromPortalRelease, ToPortalRelease, FromAppRelease, ToAppRelease FROM BUSDTA.SecurableFeatures");
                DataSet dsSecurableFeature = DbHelper.ExecuteDataSet(MylstSecurableFeatureQuery.ToString(), "SystemDB");
                List<SecurableFeature> lstSecurableFeature = new List<SecurableFeature>();
                if (dsSecurableFeature.HasData())
                {
                    lstSecurableFeature = dsSecurableFeature.GetEntityList<SecurableFeature>();
                }
                return lstSecurableFeature;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SecurableFeature> GetSpecificSecurableFeatureList(string FeatureID, string FeatureName, string Description)
        {
            try
            {
                logger.Info("SecurableFeatureManager GetSpecificSecurableFeatureList");
                StringBuilder MylstSecurableFeatureQuery = new StringBuilder();
                MylstSecurableFeatureQuery.Append("SELECT FeatureID, FeatureName, FeatureDescription, DefaultValue, OptionType, OptionValue, FromPortalRelease, ToPortalRelease, FromAppRelease, ToAppRelease FROM BUSDTA.SecurableFeatures WHERE 1=1 ");
                if (string.IsNullOrEmpty(FeatureID) != true && FeatureID.Trim() != "")
                    MylstSecurableFeatureQuery.Append("AND FeatureID = " + FeatureID.Trim());
                if (string.IsNullOrEmpty(FeatureName) != true && FeatureName.Trim() != "")
                    MylstSecurableFeatureQuery.Append("AND FeatureName LIKE '%" + FeatureName + "%'");
                if (string.IsNullOrEmpty(Description) != true && Description.Trim() != "")
                    MylstSecurableFeatureQuery.Append("AND FeatureDescription LIKE '%" + Description + "%'");
                DataSet dsSecurableFeature = DbHelper.ExecuteDataSet(MylstSecurableFeatureQuery.ToString(), "SystemDB");
                List<SecurableFeature> lstSecurableFeature = new List<SecurableFeature>();
                if (dsSecurableFeature.HasData())
                {
                    lstSecurableFeature = dsSecurableFeature.GetEntityList<SecurableFeature>();
                }
                return lstSecurableFeature;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertNewSecurableFeature(SecurableFeature model)
        {
            string MyQuery = string.Empty;
            int result = -1;
            try
            {
                MyQuery = "SELECT COUNT(*) FROM BUSDTA.SecurableFeatures rm WHERE rm.FeatureName='{0}'";
                MyQuery = string.Format(MyQuery, model.FeatureName);
                result = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
                if (result == 0)
                {

                    if (model.SelectedOption == "Toggle")
                        model.DefaultValue = model.SelectedToggleValue;

                    MyQuery = "INSERT INTO BUSDTA.SecurableFeatures(FeatureName,FeatureDescription,OptionType,OptionValue,DefaultValue,FromPortalRelease,ToPortalRelease,CreatedBy,CreatedDate) OUTPUT INSERTED.FeatureID VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',1,GETDATE())";
                    MyQuery = string.Format(MyQuery, model.FeatureName, model.FeatureDescription, model.SelectedOption, model.OptionValue, model.DefaultValue, model.FromPortalRelease, model.ToPortalRelease);
                    result = Convert.ToInt32(DbHelper.ExecuteScalar(MyQuery));
                    if (result > 0)
                    {
                        model.FeatureID = Convert.ToString(result);
                        InsertNewRoleFeature(model);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result > 0 ? true : false;
        }

        private bool InsertNewRoleFeature(SecurableFeature model)
        {
            string MyQuery = string.Empty;
            int result = -1;
            try
            {
                AdminManager adminModel = new AdminManager();
                List<AdminRole> lstAdminRole = new List<AdminRole>();
                lstAdminRole = adminModel.GetRoleList();
                if (model.SelectedOption == "Toggle")
                    model.DefaultValue = model.DefaultValue.ToLower() == "true" ? "1" : "0";
                foreach (var objRole in lstAdminRole)
                {
                    MyQuery = "insert into BUSDTA.RoleFeatures (RoleId, FeatureID, SettingValue, CreatedDate, CreatedBy) values (" + objRole.RoleID + "," + model.FeatureID + ",'" + model.DefaultValue + "', '" + DateTime.Now + "', 1)";
                    DbHelper.ExecuteNonQuery(MyQuery);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result > 0 ? true : false;
        }

        public bool UpdateFeatureType(string state, string roleFeatureID)
        {
            int result = -1;
            try
            {
                logger.Info("SecurableFeatureManager UpdateFeatureType Parameters: " + state + " " + roleFeatureID);
                string MyQuery = string.Empty;
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                MyQuery = "UPDATE BUSDTA.SecurableFeatures SET OptionType=" + toggleTo + " WHERE FeatureID='" + roleFeatureID + "'";
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager UpdateDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }


        public bool UpdateFeatureValue(string state, string roleFeatureID)
        {
            int result = -1;
            try
            {
                logger.Info("SecurableFeatureManager UpdateFeatureValue Parameters: " + state + " " + roleFeatureID);
                string MyQuery = string.Empty;
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                MyQuery = "UPDATE BUSDTA.SecurableFeatures SET OptionValue=" + toggleTo + " WHERE FeatureID='" + roleFeatureID + "'";
                result = DbHelper.ExecuteNonQuery(MyQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager UpdateDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public IEnumerable<SelectListItem> GetOptionType()
        {
            List<SelectListItem> lstOptionType = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RoleFeatureManager GetRoleSelectableList");
                lstOptionType.Add(new SelectListItem { Text = "Toggle", Value = "Toggle" });
                lstOptionType.Add(new SelectListItem { Text = "List", Value = "List" });
                return new SelectList(lstOptionType, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeature GetRoleSelectableList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetToggleList()
        {
            List<SelectListItem> lstToggleList = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RoleFeatureManager GetRoleSelectableList");
                lstToggleList.Add(new SelectListItem { Text = "True", Value = "True" });
                lstToggleList.Add(new SelectListItem { Text = "False", Value = "False" });
                return new SelectList(lstToggleList, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RoleFeature GetRoleSelectableList" + ex.Message);
                throw ex;
            }
        }

    }
}