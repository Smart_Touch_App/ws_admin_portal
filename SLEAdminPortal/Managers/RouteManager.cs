﻿using System;
using System.Collections.Generic;
using SLEAdminPortal.Helpers;
using System.Data;
using SLEAdminPortal.Models;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
namespace SLEAdminPortal.Managers
{
    public class RouteManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Route> GetRouteList()
        {
            try
            {
                logger.Info("RouteManager GetRouteList");
                string query = string.Empty;
                query = query + "select distinct ";
                query = query + "r.FFUSER as 'RouteID', ";
                query = query + "r.FFROUT as 'RouteNo', ";
                query = query + "r.FFMCU as 'Branch', ";
                query = query + "descp.DRDL01 as 'Description' ";
                query = query + "from BUSDTA.F56M0001 r left outer join ";
                query = query + "BUSDTA.F0005 descp on LTRIM(rtrim(descp.DRKY))= LTRIM(rtrim(r.FFROUT))";
                query = query + "where descp.DRSY = '42' and descp.DRRT = 'RT' ";
                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    routes = dsRoute.GetEntityList<Route>();
                }
                return routes;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetRouteSelectableList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RouteManager GetRouteSelectableList");
                query = query + "select distinct ";
                query = query + "r.FFUSER as 'RouteID', ";
                query = query + "r.FFROUT as 'RouteNo', ";
                query = query + "r.FFMCU as 'Branch', ";
                query = query + "descp.DRDL01 as 'Description' ";
                query = query + "from BUSDTA.F56M0001 r left outer join ";
                query = query + "BUSDTA.F0005 descp on LTRIM(rtrim(descp.DRKY))= LTRIM(rtrim(r.FFROUT))";
                query = query + "where descp.DRSY = '42' and descp.DRRT = 'RT' ";
                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    foreach (DataRow route in dsRoute.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = route["RouteID"].ToString(), Value = route["RouteID"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteSelectableList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetRouteSelectableListNew()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RouteManager GetRouteSelectableList");
                query = query + "select distinct ";
                query = query + "r.RouteMasterID as 'RouteID', ";
                query = query + "r.RouteName as 'RouteName' ";
                query = query + "from BUSDTA.Route_Master r Order by r.RouteName";

                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    list.Add(new SelectListItem { Text = "All Routes", Value = "All" });
                    foreach (DataRow route in dsRoute.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = route["RouteName"].ToString(), Value = route["RouteID"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteSelectableList" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetTypeSelectableListNew()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RouteManager GetTypeSelectableListNew");
                query = query + "select distinct ";
                query = query + "t.TTID as 'TTID', ";
                query = query + "t.TTADSC as 'TTADSC' ";

                query = query + "from BUSDTA.M5001 t where t.TTTYP='Replenishment'";

                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    list.Add(new SelectListItem { Text = "All Repln.", Value = "All" });
                    foreach (DataRow route in dsRoute.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = route["TTADSC"].ToString(), Value = route["TTID"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetTypeSelectableListNew" + ex.Message);
                throw ex;
            }
        }

        public IEnumerable<SelectListItem> GetReasonSelectableListNew()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RouteManager GetRouteSelectableList");
                query = query + "select distinct ";
                query = query + "r.ReasonCodeId as 'ReasonId', ";
                query = query + "r.ReasonCodeDescription as 'Reason' ";

                query = query + "from BUSDTA.ReasonCodeMaster r where r.ReasonCodeType='Void Cycle Count'";


                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    foreach (DataRow route in dsRoute.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = route["Reason"].ToString(), Value = route["ReasonId"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteSelectableList" + ex.Message);
                throw ex;
            }
        }

        public List<Device> GetDeviceList()
        {
            try
            {
                logger.Info("RouteManager GetDeviceList");
                //string query = string.Empty, query1 = string.Empty, 
                string query2 = string.Empty;
                //string AppUser = string.Empty,Device_ID = string.Empty;
                //query = query + " SELECT App_user_id FROM BUSDTA.user_master UM ";
                //query = query + " INNER JOIN  BUSDTA.User_Environment_Map UEM ON UEM.AppUserID = UM.App_User_ID ";
                //query = query + " WHERE UM.DomainUser =LTRIM(RTRIM('" + UserName + "')) ";
                //DataSet dsDevice = DbHelper.ExecuteDataSet(query, "SystemDB");
                //List<Device> devices = new List<Device>();
                //if (dsDevice.HasData())
                //{
                //    devices = dsDevice.GetEntityList<Device>();
                //}
                //foreach (var id in devices)
                //{
                //    AppUser += id.App_user_id + ",";
                //}
                //AppUser = AppUser.Length > 0 ? AppUser.Substring(0, AppUser.Length - 1) : "";

                //query1 = query1 + " SELECT Device_Id as 'DeviceID' FROM busdta.Route_device_map RDM ";
                //query1 = query1 + " INNER JOIN BUSDTA.ROUTE_USER_MAP RUM ON RUM.ROUTE_ID = RDM.ROUTE_ID ";
                //query1 = query1 + " where RUM.APP_uSER_ID in('"+ AppUser+"')";
                //DataSet dsDevice1 = DbHelper.ExecuteDataSet(query1, "SLECons_Dev");
                //if (dsDevice1.HasData())
                //{
                //    devices = dsDevice1.GetEntityList<Device>();
                //}
                //foreach (var id in devices)
                //{
                //    Device_ID += id.DeviceID + ",";
                //}
                //Device_ID = Device_ID.Length > 0 ? Device_ID.Substring(0, Device_ID.Length - 1) : "";

                //query2 = query2 + " SELECT LTRIM(RTRIM(Device_Id)) AS 'MachineID',DeviceName AS 'MachineName', ";
                //query2 = query2 + " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.Device_Environment_Map DEM ";
                //query2 = query2 + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
                //query2 = query2 + " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'Environment',  ";
                //query2 = query2 + " RDM.RouteID AS Route ,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status, Active  ";
                //query2 = query2 + " FROM BUSDTA.Device_Master DM ";
                //query2 = query2 + " LEFT JOIN BUSDTA.RemoteDBID_Master RDM ON RDM.DeviceID = DM.Device_Id ";
                //query2 = query2 + " GROUP BY Device_Id, DeviceName,RouteID,ActiveYN,Active,dm.last_modified";
                //query2 = query2 + " ORDER BY dm.last_modified DESC ";

                query2 = query2 + " SELECT LTRIM(RTRIM(Device_Id)) AS 'MachineID',FQDN AS 'MachineName', ";
                query2 = query2 + " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.Device_Env_Map DEM ";
                query2 = query2 + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
                query2 = query2 + " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'Environment',  ";
                query2 = query2 + " RDM.RouteID AS Route ,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status, Active  ";
                query2 = query2 + " FROM BUSDTA.MachineMaster DM ";
                query2 = query2 + " LEFT JOIN BUSDTA.RemoteDB_Master RDM ON RDM.DeviceID = DM.Device_Id ";
                query2 = query2 + " GROUP BY Device_Id, FQDN,RouteID,ActiveYN,Active,dm.last_modified";
                query2 = query2 + " ORDER BY dm.last_modified DESC ";
               

                DataSet dsDevice2 = DbHelper.ExecuteDataSet(query2, "SystemDB");
                List<Device> devices1 = new List<Device>();
                if (dsDevice2.HasData())
                {
                    devices1 = dsDevice2.GetEntityList<Device>();
                }
                return devices1;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetDeviceList" + ex.Message);
                throw ex;
            }
        }
        public List<Device> DeviceDeregister(string MachineID, string Route)
        {
            try
            {
                string ConnectionString = string.Empty;
                ConnectionString = ConfigurationManager.ConnectionStrings["SystemDB"].ToString();
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                SqlCommand cmd = new SqlCommand(ConnectionString);
                Int32 rowsAffected;
                cmd.CommandText = "uspDeregisterRouteDeviceV1";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = MachineID;
                cmd.Parameters.Add("@RouteName", SqlDbType.VarChar).Value = Route;
                cmd.Connection = dbConnection;
                dbConnection.Open();
                rowsAffected = cmd.ExecuteNonQuery();
                dbConnection.Close();
                List<Device> devices = new List<Device>();
                return devices;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager DeviceDeregister" + ex.Message);
                throw ex;
            }

        }
        public List<Device> DeviceInfo(string MachineID)
        {
            string query = string.Empty;
            query = query + " SELECT EM.EnvName as 'Environment',DM.FQDN as Manufacturer,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status ";
            query = query + " FROM BUSDTA.MachineMaster DM ";
            query = query + " INNER JOIN BUSDTA.Device_Env_Map DEM ON DEM.DeviceId = DM.Device_Id ";
            query = query + " INNER JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
            query = query + " LEFT JOIN BUSDTA.RemoteDB_Master RDM ON RDM.DeviceID = DM.Device_Id ";
            query = query + " WHERE Device_Id = '"+ MachineID +"'";
            DataSet dsDeviceInfo = DbHelper.ExecuteDataSet(query, "SystemDB");
            List<Device> DeviceInfo = new List<Device>();
            if (dsDeviceInfo.HasData())
            {
                DeviceInfo = dsDeviceInfo.GetEntityList<Device>();
            }
            return DeviceInfo;
        }
        public string ChangeActiveFlag(string MachineID,string Active)
        {
            try
            {
                int result = -1;
                string query = string.Empty;
                string userID = string.Empty;
                if(Active == "0")
                {
                    query = query + " UPDATE BUSDTA.MachineMaster SET Active = 1 WHERE Device_Id ='" + MachineID + "' ";
                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");
                }
                else if(Active == "1")
                {
                    query = query + " UPDATE BUSDTA.MachineMaster SET Active = 0 WHERE Device_Id ='" + MachineID + "' ";
                    result = DbHelper.ExecuteNonQuery(query, null, false, "SystemDB");
                }
                return userID;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager DeviceDeregister" + ex.Message);
                throw ex;
            }

        }

        public List<Device> GetSpecificDeviceList(string DeviceID, string MachineName, string Environment)
        {
            try
            {
                logger.Info("RouteManager GetSpecificDeviceList");
                string query = string.Empty;
                query = query + " SELECT LTRIM(RTRIM(Device_Id)) AS 'MachineID',FQDN AS 'MachineName', ";
                query = query + " STUFF((SELECT ', ' + CDBConKey  FROM BUSDTA.Device_Env_Map DEM ";
                query = query + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId ";
                query = query + " WHERE DEM.DeviceId = DM.Device_Id FOR XML PATH('')), 1, 2, '') AS 'Environment',  ";
                query = query + " RDM.RouteID AS Route ,CASE RDM.ActiveYN when 'Y' THEN 'Onboarded' when 'N' THEN 'Reserved' ELSE 'Ready To Onboard' END AS Status, Active  ";
                query = query + " FROM BUSDTA.MachineMaster DM ";
                query = query + " LEFT JOIN BUSDTA.RemoteDB_Master RDM ON RDM.DeviceID = DM.Device_Id ";
                query = query + " LEFT JOIN BUSDTA.Device_Env_Map DEM on DEM.EnvironmentMasterId = RDM.EnvironmentMasterID ";
                query = query + " LEFT JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = DEM.EnvironmentMasterId  ";
                query = query + " where 1=1 ";
                if (!string.IsNullOrEmpty(DeviceID))
                {
                    query = query + " and Device_Id='" + DeviceID + "'";
                }
                if (!string.IsNullOrEmpty(MachineName))
                {
                    query = query + " and FQDN='" + MachineName + "'";
                }
                if (!string.IsNullOrEmpty(Environment))
                {
                    query = query + " and EM.CDBConKey='" + Environment + "'";
                }
                query = query + " GROUP BY Device_Id, FQDN,RouteID,ActiveYN,Active,dm.last_modified";
                query = query + " ORDER BY dm.last_modified DESC ";
                DataSet dsDevice = DbHelper.ExecuteDataSet(query, "SystemDB");
                List<Device> devices = new List<Device>();
                if (dsDevice.HasData())
                {
                    devices = dsDevice.GetEntityList<Device>();
                }
                return devices;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetSpecificDeviceList" + ex.Message);
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> GetEnvironment()
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                string query = string.Empty;
                query = query + "select EnvName from BUSDTA.Environment_Master";
                DataSet dsEnvironment= DbHelper.ExecuteDataSet(query, "SystemDB");
                List<SLEAdminPortal.Models.Environment> environment = new List<SLEAdminPortal.Models.Environment>();
                if (dsEnvironment.HasData())
                {
                    foreach (DataRow envmnt in dsEnvironment.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = envmnt["EnvName"].ToString(), Value = envmnt["EnvName"].ToString() });
                    }
                }
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetEnvironment" + ex.Message);
                throw ex;
            }
        }
    }
}